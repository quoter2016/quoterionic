<?php

require_once('SQLHelper.php');

class Preferences {

	private $sql_obj = null;

	public function __construct(){

		$this->sql_obj = SQLHelper::get_instance();
	}

	public function changePassword($id,$newpass){
		$sql = "
			CALL changePassword($id,'$newpass')
		";

		if($this->sql_obj->CALL($sql)){
			$return['status'] = "SUCCESS";
		} else {
			$return['status'] = "FAILED";
		}
		echo json_encode($return);
		
	}

	public function changeLocation($id,$lat,$lng){
		$sql = "
			CALL changePassword($id,$lat,$lng)
		";

		if($this->sql_obj->CALL($sql)){
			$return['status'] = "SUCCESS";
		} else {
			$return['status'] = "FAILED";
		}

		echo json_encode($return);

	}

	public function isExpire($id){
		$cur = date('Y-m-d H:i:s');
		$sql = "
			SELECT * 
			FROM qtr_access
			WHERE id = $id
		";

		$res = $this->sql_obj->SELECT($sql);

		$row = mysqli_fetch_array($res);
		$return = array();

		if($row['expire'] < $cur){
			$return['expire'] = true;
		} else {
			$return['expire'] = false;
		}

		echo json_encode($return);
	}

	public function keepSignedIn($access_token){

	}

	public function logout($id){
		$return['status'] = true;
		$sql = "UPDATE qtr_users
				SET active = 0
				WHERE id = $id";

		$res = $this->sql_obj->SELECT($sql);

		$sql = "UPDATE qtr_access
				SET online = 0
				WHERE user_id = $id
				";
		$res = $this->sql_obj->SELECT($sql);
		if(!$res)
			$return['status'] = false;

		echo json_encode($return);
	}
	
}




