<?php

require_once('SQLHelper.php');

class Supplier {



	private $sql_obj = null;

	public function __construct(){

		$this->sql_obj = SQLHelper::get_instance();
	}

	public function register($user){

		$sql = "
			SELECT username
			FROM qtr_users
			WHERE username = '".$user['username']."'
		";

		$res = $this->sql_obj->SELECT($sql);
		if($res->num_rows>0){
			$return['Error'] = "Username Alreay Exist";
		} else {
			$sql = "
				CALL register_supplier(
				'".$user['username']."',
				'".$user['password']."',
				'".$user['first_name']."',
				'".$user['last_name']."',
				'".$user['email']."',
				'".$user['lat']."',
				'".$user['lng']."',
				'".$user['categoryid']."',
				@p8, 'quoter')
			";

			if(!$this->sql_obj->CALL($sql)){
				$return['Status'] = "Failed";
			} else {
				$return['Status'] = "Success";
			}
		}

		echo json_encode($return);
	}

	public function login($username,$password,$rememberme){
		$user['username']=$username;
		$user['password']=$password;
		$user['rememberme'] = $rememberme;

		$sql = "
		SELECT u.id, s.categoryid as categoryid
		FROM qtr_users as u
		JOIN qtr_supplier as s
		ON u.id = s.userid
		WHERE u.username = '".$user['username']."'
		AND u.password = '".md5($user['password'])."'
		";

		$res = $this->sql_obj->SELECT($sql);

		// return $res;
		$user = (array) mysqli_fetch_array($res);
		$return = array();
		$date = new DateTime();
		// $date->getTimestamp();
		// $t=time();

		if(!$user){
			$return['status'] = "Login Invalid";
			$return['error'] = true;

			echo json_encoDe($return);
		} else {
			$return['user_id'] = $user['id'];
			$return['token'] = MD5($user['id']."_".$date->getTimestamp());
			$return['keep_signed_in'] = $rememberme;
			$return['categoryid'] = $user['categoryid'];
			$this->addAccess($return);
		}
	}

	public function addAccess($user){
		$sql = "
			UPDATE qtr_users
			SET active = 1
			WHERE id = ".$user['user_id']."
		";

		$this->sql_obj->CALL($sql);

		$sql = "
		CALL add_access_token
		('".$user['user_id']."','".$user['token']."','".$user['keep_signed_in']."')
		";

		$return = array();
		$user1 = array();
		$res = $this->sql_obj->CALL($sql);

		$sql = "
		CALL find_supplier_access_token('".$user['token']."')
		";

		$res = $this->sql_obj->CAll($sql);

		$row = mysqli_fetch_array($res);
		if($res->num_rows > 0){
		$return['id'] = $row['id'];
		$return['access_token'] = $row['Access_Token'];
		// $row = mysqli_fetch_array($res);
				$user1['id'] = $row['User_Id'];
				$user1['username'] = $row['Username'];
				$user1['password'] = $row['Password'];
				$user1['email'] = $row['Email'];
				$user1['first_name'] = $row['First_Name'];
				$user1['last_name'] = $row['Last_Name'];
					$point['lat'] = $row['lat'];
					$point['lng'] = $row['lng'];
				$user1['location'] = $point;
				$user1['timestamp'] = $row['User_Timestamp'];
		$return['user'] = $user1;
		$return['usertype'] = $row['Usertype'];
		$return['timestamp'] = $row['Access_Timestamp'];
		$return['keep_signed_in'] = $row['Keep_Signed_In'];
		$return['categoryid'] = $user['categoryid'];
		$return['expire'] = $row['Expire'];
		}
		echo json_encode($return);
	}

	public function findNearestJobs($lat,$lng,$lim,$cat){
		$sql = "
		CALL find_nearest_jobs($lat,$lng,$lim,100,$cat)
		";
		$list = array();
		$res = $this->sql_obj->CALL($sql);

		while ($row = mysqli_fetch_array($res)) {
			//GET JOB TYPES
			$this->sql_obj->close();
			$sql="
				SELECT *
				FROM qtr_jobtypes
				WHERE id = '".$row['jobtypeid']."'
			";
			$result = $this->sql_obj->SELECT($sql);
			$result= (array) mysqli_fetch_array($result);
			$jobtype['id'] = $result['id'];
			$jobtype['description'] = $result['description'];

			//GET MEMBER
			$sql="
				SELECT *
				FROM qtr_users
				WHERE id = '".$row['memberid']."'
			";
			$result6 = $this->sql_obj->SELECT($sql);
			$result6= (array) mysqli_fetch_array($result6);
			$member['id'] = $result6['id'];
			$member['email'] = $result6['email'];
			$member['first_name'] = $result6['first_name'];
			$member['last_name'] = $result6['last_name'];
			// print_r($result6);

			

			//GET LABOURTYPE
			$sql="
				SELECT *
				FROM qtr_labourtype
				WHERE id = '".$row['labourtypeid']."'
			";
			$result2 = $this->sql_obj->SELECT($sql);
			$result2= (array) mysqli_fetch_array($result2);
			$labourtype['id'] = $result2['id'];
			$labourtype['description'] = $result2['description'];
			
			//GET BECAUSE
			$sql="
				SELECT *
				FROM qtr_because
				WHERE id = '".$row['because']."'
			";
			$result3= $this->sql_obj->SELECT($sql);
			$result3= (array) mysqli_fetch_array($result3);
			$because['id'] = $result3['id'];
			$because['description'] = $result3['description'];
			
			//GET SIZE
			$sql="
				SELECT *
				FROM qtr_size
				WHERE id = '".$row['because']."'
			";
			$result4 = $this->sql_obj->SELECT($sql);
			$result4= (array) mysqli_fetch_array($result4);
			$size['id'] = $result4['id'];
			$size['description'] = $result4['description']; 
			
			//GET START
			$sql="
				SELECT *
				FROM qtr_start
				WHERE id = '".$row['start']."'
			";
			$result5 = $this->sql_obj->SELECT($sql);
			$result5= (array) mysqli_fetch_array($result5);
			$start['id'] = $result5['id'];
			$start['description'] = $result5['description'];

			// $supplier['id'] = $row['id'];
			// $supplier['jobtypeid'] = $row['jobtypeid'];
			// $supplier['memberid'] = $row['memberid'];
			// $supplier['supplierid'] = $row['supplierid'];
			// $supplier['jobname'] = $row['jobname'];

			// $supplier['description'] = $row['description'];
			// $supplier['labourtypeid'] = $row['labourtypeid'];

			// $point['lat'] = $row['lat'];
			// $point['lng'] = $row['lng'];
			// $supplier['location'] = $point; 
			// $supplier['because'] = $row['because'];
			// $supplier['size'] = $row['size'];
			// $supplier['start'] = $row['start'];
			// $supplier['distance'] = $row['distance'];
			// $list[] = $supplier;
			$return['id'] = $row['id'];
			$return['jobtype'] = $jobtype;
			$return['member'] = $member;
			$return['jobname'] = $row['jobname'];
			$return['description'] = $row['description'];
			$return['labourtypeid'] = $labourtype;
			$point['lat'] = $row['lat'];
			$point['lng'] = $row['lng'];
			$return['location'] = $point;
			$return['because'] = $because;
			$return['size'] = $size;
			$return['start'] = $start;
			$return['timestamp'] = $row['timestamp'];
			$list[] = $return;
		}

		echo json_encode($list);
	}


//FINDS
	public function getJobsBySupplierId($searchId){
		$list = array();
		$rows = array();
		$sql = "
			CALL find_jobsBySupplierId(
				'$searchId')
		";

		$rows = $this->sql_obj->CALL($sql,true);


		foreach($rows as $row){
			// print_r($row);
			//GET JOB TYPES
			$this->sql_obj->close();
			$sql="
				SELECT *
				FROM qtr_jobtypes
				WHERE id = '".$row['jobtypeid']."'
			";
			$result = $this->sql_obj->SELECT($sql);
			$result= (array) mysqli_fetch_array($result);
			$jobtype['id'] = $result['id'];
			$jobtype['description'] = $result['description'];

			//GET MEMBER
			$sql="
				SELECT *
				FROM qtr_users
				WHERE id = '".$row['memberid']."'
			";
			$result6 = $this->sql_obj->SELECT($sql);
			$result6= (array) mysqli_fetch_array($result6);
			$member['id'] = $result6['id'];
			$member['email'] = $result6['email'];
			$member['first_name'] = $result6['first_name'];
			$member['last_name'] = $result6['last_name'];
			// print_r($result6);


			//GET SUPPLIER
			$sql="
				SELECT *
				FROM qtr_supplier
				WHERE userid = '".$row['supplierid']."'
			";
			$result1 = $this->sql_obj->SELECT($sql);
			$result1= (array) mysqli_fetch_array($result1);
			$supplier['id'] = $row['supplierid'];
			$supplier['email'] = $result6['email'];
			$supplier['first_name'] = $result6['first_name'];
			$supplier['last_name'] = $result6['last_name'];
			$spoint['lat'] = $result1['lat'];
			$spoint['lng'] = $result1['lng'];
			$supplier['location'] = $spoint;
			

			//GET LABOURTYPE
			$sql="
				SELECT *
				FROM qtr_labourtype
				WHERE id = '".$row['labourtypeid']."'
			";
			$result2 = $this->sql_obj->SELECT($sql);
			$result2= (array) mysqli_fetch_array($result2);
			$labourtype['id'] = $result2['id'];
			$labourtype['description'] = $result2['description'];
			
			//GET BECAUSE
			$sql="
				SELECT *
				FROM qtr_because
				WHERE id = '".$row['because']."'
			";
			$result3= $this->sql_obj->SELECT($sql);
			$result3= (array) mysqli_fetch_array($result3);
			$because['id'] = $result3['id'];
			$because['description'] = $result3['description'];
			
			//GET SIZE
			$sql="
				SELECT *
				FROM qtr_size
				WHERE id = '".$row['because']."'
			";
			$result4 = $this->sql_obj->SELECT($sql);
			$result4= (array) mysqli_fetch_array($result4);
			$size['id'] = $result4['id'];
			$size['description'] = $result4['description']; 
			
			//GET START
			$sql="
				SELECT *
				FROM qtr_start
				WHERE id = '".$row['start']."'
			";
			$result5 = $this->sql_obj->SELECT($sql);
			$result5= (array) mysqli_fetch_array($result5);
			$start['id'] = $result5['id'];
			$start['description'] = $result5['description'];
			


			$return['id'] = $row['id'];
			$return['jobtype'] = $jobtype;
			$return['supplier'] = $supplier;
			$return['member'] = $member;
			$return['jobname'] = $row['jobname'];
			$return['description'] = $row['description'];
			$return['labourtypeid'] = $labourtype;
			$point['lat'] = $row['lat'];
			$point['lng'] = $row['lng'];
			$return['location'] = $point;
			$return['because'] = $because;
			$return['size'] = $size;
			$return['start'] = $start;
			$list[] = $return;
		}
		echo json_encode($list);
	}

	


}




