<?php

require_once('SQLHelper.php');

class Messages {

	private $sql_obj = null;

	public function __construct(){

		$this->sql_obj = SQLHelper::get_instance();
	}

	public function getMessagesByMemberId($memberid){
		$sql = "
			CALL find_messagesByMemberId($memberid);
		";

		$list = array();

		$res = $this->sql_obj->CALL($sql);
		$groups = array();
		while ($row = mysqli_fetch_array($res)){
			$msg = array();
			$msg['id'] = $row['id'];
			$msg['memberid'] = $row['memberid'];
			$msg['supplierid'] = $row['supplierid'];
			$msg['sender'] = $row['sender'];
			$msg['content'] = $row['content'];
			$msg['jobid'] = $row['jobid'];
			$msg['timestamp'] = $row ['timestamp'];
			$supplier['supplier_name'] = $row['fname']." ".$row['lname'];
			$supplier['supplier_email'] = $row['email'];
			$supplier['supplier_username'] = $row['username'];

			if(!isset($groups[$supplier['supplier_username']])) {
				$groups[$supplier['supplier_username']] = array();
				$groups[$supplier['supplier_username']]['info'] = $supplier;
				$groups[$supplier['supplier_username']]['messages'] = array();
			} 

			array_push($groups[$supplier['supplier_username']]['messages'], $msg);
		}

		foreach ($groups as $group) {
			$list[] = $group;
		}

		echo json_encode($list);
	}

	public function getMessagesByMemberIdSort($memberid){
		$sql = "
			CALL find_messagesByMemberId($memberid);
		";

		$list = array();

		$res = $this->sql_obj->CALL($sql);
		$groups = array();
		while ($row = mysqli_fetch_array($res)){
			$msg = array();
			$msg['id'] = $row['id'];
			$msg['memberid'] = $row['memberid'];
			$msg['supplierid'] = $row['supplierid'];
			$msg['sender'] = $row['sender'];
			$msg['content'] = $row['content'];
			$msg['jobid'] = $row['jobid'];
			$msg['timestamp'] = $row ['timestamp'];
			$supplier['supplier_name'] = $row['fname']." ".$row['lname'];
			$supplier['supplier_email'] = $row['email'];
			$supplier['supplier_username'] = $row['username'];

			if(!isset($groups[$supplier['supplier_username']])) {
				$groups[$supplier['supplier_username']] = array();
				$groups[$supplier['supplier_username']]['info'] = $supplier;
				$groups[$supplier['supplier_username']]['messages'] = array();
			} 

			array_push($groups[$supplier['supplier_username']]['messages'], $msg);
		}

		foreach ($groups as $group) {
			$list[] = $group;
		}

		echo json_encode($list);
	}

	public function getMessagesBySupplierid($supplierid){
		$sql = "
			CALL find_messagesBySupplierId($supplierid);
		";
		$list = array();

		$res = $this->sql_obj->CALL($sql);
		$groups = array();
		while ($row = mysqli_fetch_array($res)){
			$msg = array();
			$msg['id'] = $row['id'];
			$msg['memberid'] = $row['memberid'];
			$msg['supplierid'] = $row['supplierid'];
			$msg['sender'] = $row['sender'];
			$msg['content'] = $row['content'];
			$msg['jobid'] = $row['jobid'];
			$msg['timestamp'] = $row ['timestamp'];
			$member['member_name'] = $row['fname']." ".$row['lname'];
			$member['member_email'] = $row['email'];
			$member['member_username'] = $row['username'];

			if(!isset($groups[$member['member_username']])) {
				$groups[$member['member_username']] = array();
				$groups[$member['member_username']]['info'] = $member;
				$groups[$member['member_username']]['messages'] = array();
			} 

			array_push($groups[$member['member_username']]['messages'], $msg);
		}

		foreach ($groups as $group) {
			$list[] = $group;
		}

		echo json_encode($list);
	}

	public function getMessagesBySupplieridSort($supplierid){
		$sql = "
			CALL find_messagesBySupplierId($supplierid);
		";
		$list = array();

		$res = $this->sql_obj->CALL($sql);
		$groups = array();
		while ($row = mysqli_fetch_array($res)){
			$msg = array();
			$msg['id'] = $row['id'];
			$msg['memberid'] = $row['memberid'];
			$msg['supplierid'] = $row['supplierid'];
			$msg['sender'] = $row['sender'];
			$msg['content'] = $row['content'];
			$msg['jobid'] = $row['jobid'];
			$msg['timestamp'] = $row ['timestamp'];
			$member['member_name'] = $row['fname']." ".$row['lname'];
			$member['member_email'] = $row['email'];
			$member['member_username'] = $row['username'];

			if(!isset($groups[$member['member_username']])) {
				$groups[$member['member_username']] = array();
				$groups[$member['member_username']]['info'] = $member;
				$groups[$member['member_username']]['messages'] = array();
			} 

			array_push($groups[$member['member_username']]['messages'], $msg);
		}

		foreach ($groups as $group) {
			$list[] = $group;
		}

		echo json_encode($list);
	}

	public function getMessagesByJobId($jobid){

		$sql = "
			CALL find_messagesByJobId($jobid);
		";

		$res = $this->sql_obj->CALL($sql);
		while ($row = mysqli_fetch_array($res)){
			$return['id'] = $row['id'];
			$return['memberid'] = $row['memberid'];
			$return['supplierid'] = $row['supplierid'];
			$return['sender'] = $row['sender'];
			$return['content'] = $row['content'];
			$return['jobid'] = $row['jobid'];
			$return['timestamp'] = $row ['timesent'];
			$list[] = $return;
		}

		echo json_encode($list);
	}

	public function getMessagesByPartner($memberid,$supplierid){

		$list = array();
		$sql = "
			CALL find_messagesByPartner($memberid,$supplierid)
		";

		$res = $this->sql_obj->CALL($sql);

		while ($row = mysqli_fetch_array($res)){
			$return['id'] = $row['id'];
			$return['memberid'] = $row['memberid'];
			$return['supplierid'] = $row['supplierid'];
			$return['sender'] = $row['sender'];
			$return['content'] = $row['content'];
			$return['jobid'] = $row['jobid'];
			$return['timestamp'] = $row ['timestamp'];
			$list[] = $return;
		}

		echo json_encode($list);
	}


	public function sendMessage($memberid,$supplierid,$sender,$content,$jobid){

		$sql ="
			CALL send_message($memberid,$supplierid,$sender,'$content',$jobid)
		";

		if(!$this->sql_obj->CALL($sql)){
			$return['Status'] = "Failed";
		} else {
			$return['Status'] = "Success";
	    }
		echo json_encode($return);
	}
}




