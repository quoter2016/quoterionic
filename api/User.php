<?php

require_once('SQLHelper.php');

class User {

	private $sql_obj = null;

	public function __construct(){

		$this->sql_obj = SQLHelper::get_instance();
	}

	public function testuid($uid){
		$return = array();


//		$return["result"] = "user data dummy";

		$sql = "
			CALL find_memberById(
				".$uid."
				)
		";

		$list = array();
		$res = $this->sql_obj->CALL($sql);

		while ($row = mysqli_fetch_array($res)) {
			$return['username'] = $row['username'];
			$return['useremail'] = $row['email'];
		}

//		if($this->sql_obj->CALL($sql)){
//			$return['Status'] = "SUCCESS";
//		} else {
//			$return['Status'] = "FAILED";
//		}

		echo json_encode($return);
	}

	public function test_getUserByEmailDomain($domain){
		$return = array();

//		$return["username"] = "user data dummy";

$d = "%" . $domain . "%";

		$sql = "
			CALL find_memberByEmailDomain(
				'".$d."'
				)
		";

		$list = array();
		$res = $this->sql_obj->CALL($sql);

		while ($row = mysqli_fetch_array($res)) {
			$line = array();
			$line['username'] = $row['username'];
			$list[] = $line;
		}

		echo json_encode($list);
	}



	public function wayne(){

		$return = array();

		$return['name'] = "Wayne";
		$return['location'] = "Pattay";

		echo json_encode($return);
	}


	public function register($user){
		$return = array();
		$sql = "
		SELECT username
		FROM qtr_users
		WHERE username = '".$user['username']."'
		";
		$res = $this->sql_obj->SELECT($sql);
		if($res->num_rows>0){
			$return['Error'] = "Username Alreay Exist";
		} else {
			$sql = "
			CALL register_member(
			'".$user['username']."',
			'".$user['password']."',
			'".$user['first_name']."',
			'".$user['last_name']."',
			'".$user['email']."', 
			'quoter')
			";

			if(!$this->sql_obj->CALL($sql)){
				$return['Error'] = "Failed To Register";
			} else {
				$return['Status'] = "Success";
			}

		}

		echo json_encode($return);
	}

	public function login($username,$password,$keep_signed_in){
		$user['username']=$username;
		$user['password']=$password;


		$sql = "
		SELECT *
		FROM qtr_users
		WHERE username = '".$user['username']."'
		AND password = '".md5($user['password'])."'
		";

		$res = $this->sql_obj->SELECT($sql);

		// return $res;
		$user = (array) mysqli_fetch_array($res);
		// print_r($user);
		$return = array();
		$date = new DateTime();
		// $date->getTimestamp();
		// $t=time();

		if(!$user){
			$return['status'] = "Login Invalid";
			$return['error'] = true;

			echo json_encoDe($return);
		} else {
				
			$sql = "
				SELECT * FROM qtr_supplier
				WHERE userid = '".$user['id']."'
			";

			$result = $this->sql_obj->SELECT($sql);
			if(!($result->num_rows >0)){
				$return['user_id'] = $user['id'];
				$return['token'] = MD5($user['id']."_".$date->getTimestamp());
				$return['keep_signed_in'] = $keep_signed_in;
				$this->addAccess($return);
			} else {
				$return['status'] = "Login Invalid";
				$return['error'] = true;
				echo json_encoDe($return);
			}

			
		}
		
	}


	public function isExpired($access_token){
		$return = array();
		$sql = "
		SELECT access_token
		FROM qtr_access
		WHERE access_token = $access_token
		AND timestamp < expire;
		";

		$res = $this->sql_obj->SELECT();

		if($res->num_rows >0){
			$return['Status'] = "Token is Valid";
		} else {
			$return['Status'] = "Token has Already Expired";

		}
		echo json_encode($return);
	}

	public function addJob($jobtypeid, $memberid, $jobname, $description, $labourtypeid, $lat, $long, $because, $size, $start){
		$sql = "
				CALL add_job(
					'".$jobtypeid."',
					'".$memberid."',
					'".$jobname."',
					'".$description."',
					'".$labourtypeid."',
					'".$lat."',
					'".$long."',
					'".$because."',
					'".$size."',
					'".$start."'
					)
		";

		if($this->sql_obj->CALL($sql)){
			$return['Status'] = "SUCCESS";
		} else {
			$return['Status'] = "FAILED";
		}

		echo json_encode($return);
	}

	public function addAccess($user){
		$sql = "
		CALL add_access_token
		('".$user['user_id']."','".$user['token']."','".$user['keep_signed_in']."')
		";
		// print_r($user);
		$return = array();
		$user1 = array();
		$res = $this->sql_obj->CALL($sql);

		$sql = "
		CALL find_user_access_token('".$user['token']."')
		";

		$res = $this->sql_obj->CAll($sql);

		$row = mysqli_fetch_array($res);
		// print_r($row);
		if($res->num_rows > 0){
		$return['id'] = $row['id'];
		$return['access_token'] = $row['Access_Token'];
		// $row = mysqli_fetch_array($res);
				$user1['id'] = $row['User_Id'];
				$user1['username'] = $row['Username'];
				$user1['password'] = $row['Password'];
				$user1['email'] = $row['Email'];
				$user1['first_name'] = $row['First_Name'];
				$user1['last_name'] = $row['Last_Name'];
				$user1['timestamp'] = $row['User_Timestamp'];
		$return['user'] = $user1;
		$return['usertype'] = $row['Usertype'];
		$return['timestamp'] = $row['Access_Timestamp'];
		$return['keep_signed_in'] = $row['Keep_Signed_In'];
		$return['expire'] = $row['Expire'];
		}
		echo json_encode($return);

	}

	public function findNearestSupplier($lat,$lng,$lim,$cat){
		$sql = "
		CALL find_nearest_suppliers($lat,$lng,$lim,100,$cat)
		";
		$list = array();
		$res = $this->sql_obj->CALL($sql);

		while ($row = mysqli_fetch_array($res)) {
			$supplier['id'] = $row['id'];
			$supplier['first_name'] = $row['first_name'];
			$supplier['last_name'] = $row['last_name'];
			$supplier['email'] = $row['email'];
			$supplier['lat'] = $row['lat'];
			$supplier['lng'] = $row['lng'];
			$supplier['distance'] = $row['distance'];
			$list[] = $supplier;
		}

		echo json_encode($list);
	}

	public function bookBySupplierId($jobId,$supId){
		$sql= "
			CALL book_by_supplierid($jobId,$supId)
		";
		$return = array();
		$res = $this->sql_obj->CALL($sql);

		if(!$res){
			$return['status'] = "FAILED";
		} else {
			$return['status'] = "SUCCESS";
		}

		echo json_encode($return);
	}

	public function getJobsByUserId($searchId){
		$list = array();
		$rows = array();
		$sql = "
			CALL find_jobsByUserId(
				'$searchId')
		";

		$rows = $this->sql_obj->CALL($sql,true);


		foreach($rows as $row){

			//GET JOB TYPES
			$this->sql_obj->close();
			$sql="
				SELECT *
				FROM qtr_jobtypes
				WHERE id = '".$row['jobtypeid']."'
			";

			$result = $this->sql_obj->SELECT($sql);
			$result= (array) mysqli_fetch_array($result);
			$jobtype['id'] = $result['id'];
			$jobtype['description'] = $result['description'];
			// print_r($jobtype);
			//GET SUPPLIER
			$sql="
				SELECT *
				FROM qtr_users
				WHERE id = '".$row['supplierid']."'
			";
			$result1 = $this->sql_obj->SELECT($sql);
			$result1= (array) mysqli_fetch_array($result1);
			$supplier['id'] = $result1['id'];
			$supplier['email'] = $result1['email'];
			$supplier['first_name'] = $result1['first_name'];
			$supplier['last_name'] = $result1['last_name'];

			$sql="
				SELECT *
				FROM qtr_supplier
				WHERE userid = '".$row['supplierid']."'
			";
			$result7 = $this->sql_obj->SELECT($sql);
			$result7= (array) mysqli_fetch_array($result7);
			$spoint['lat'] = $result7['lat'];
			$spoint['lng'] = $result7['lng'];
			$supplier['location'] = $spoint;
			// print_r($supplier);

			//GET LABOURTYPE
			$sql="
				SELECT *
				FROM qtr_labourtype
				WHERE id = '".$row['labourtypeid']."'
			";
			$result2 = $this->sql_obj->SELECT($sql);
			$result2= (array) mysqli_fetch_array($result2);
			$labourtype['id'] = $result2['id'];
			$labourtype['description'] = $result2['description'];
			
			//GET BECAUSE
			$sql="
				SELECT *
				FROM qtr_because
				WHERE id = '".$row['because']."'
			";
			$result3= $this->sql_obj->SELECT($sql);
			$result3= (array) mysqli_fetch_array($result3);
			$because['id'] = $result3['id'];
			$because['description'] = $result3['description'];
			
			//GET SIZE
			$sql="
				SELECT *
				FROM qtr_size
				WHERE id = '".$row['because']."'
			";
			$result4 = $this->sql_obj->SELECT($sql);
			$result4= (array) mysqli_fetch_array($result4);
			$size['id'] = $result4['id'];
			$size['description'] = $result4['description']; 
			
			//GET START
			$sql="
				SELECT *
				FROM qtr_start
				WHERE id = '".$row['start']."'
			";
			$result5 = $this->sql_obj->SELECT($sql);
			$result5= (array) mysqli_fetch_array($result5);
			$start['id'] = $result5['id'];
			$start['description'] = $result5['description'];
			


			$return['id'] = $row['id'];
			$return['jobtype'] = $jobtype;
			$return['supplier'] = $supplier;
			$return['jobname'] = $row['jobname'];
			$return['description'] = $row['description'];
			$return['labourtypeid'] = $labourtype;
			$point['lat'] = $row['lat'];
			$point['lng'] = $row['lng'];
			$return['location'] = $point;
			$return['because'] = $because;
			$return['size'] = $size;
			$return['start'] = $start;
			$return['timestamp'] = $row['timestamp'];
			$list[] = $return;
		}
		echo json_encode($list);
	}

	
}




