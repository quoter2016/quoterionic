<?php

require_once('SQLHelper.php');

class Messages {

	private $sql_obj = null;

	public function __construct(){

		$this->sql_obj = SQLHelper::get_instance();
	}

	public function getMessagesByMemberId($memberid){
		$sql = "
			CALL find_messagesByMemberId($memberid);
		";

		$list = array();

		$res = $this->sql_obj->CALL($sql);
		while ($row = mysqli_fetch_array($res)){
			$return['id'] = $row['id'];
			$return['memberid'] = $row['memberid'];
			$return['supplierid'] = $row['supplierid'];
			$return['sender'] = $row['sender'];
			$return['content'] = $row['content'];
			$return['jobid'] = $row['jobid'];
			$return['timestamp'] = $row ['timesent'];
			$return['sender_name'] = $row['fname']." ".$row['lname'];
			$return['sender_email'] = $row['email'];
			$return['sender_username'] = $row['username'];
			$list[] = $return;
		}

		echo json_encode($list);
	}

	public function getMessagesBySupplierid($supplierid){
		$sql = "
			CALL find_messagesBySupplierId($supplierid);
		";

		$res = $this->sql_obj->CALL($sql);
		while ($row = mysqli_fetch_array($res)){
			$return['id'] = $row['id'];
			$return['memberid'] = $row['memberid'];
			$return['supplierid'] = $row['supplierid'];
			$return['sender'] = $row['sender'];
			$return['content'] = $row['content'];

			$return['jobid'] = $row['jobid'];

			$return['timestamp'] = $row ['timesent'];
			$return['sender_name'] = $row['fname']." ".$row['lname'];
			$return['sender_email'] = $row['email'];
			$return['sender_username'] = $row['username'];
			$list[] = $return;
		}

		echo json_encode($list);
	}

	public function getMessagesByJobId($jobid){

		$sql = "
			CALL find_messagesByJobId($jobid);
		";

		$res = $this->sql_obj->CALL($sql);
		while ($row = mysqli_fetch_array($res)){
			$return['id'] = $row['id'];
			$return['memberid'] = $row['memberid'];
			$return['supplierid'] = $row['supplierid'];
			$return['sender'] = $row['sender'];
			$return['content'] = $row['content'];
			$return['jobid'] = $row['jobid'];
			$return['timestamp'] = $row ['timestamp'];
			$list[] = $return;
		}

		echo json_encode($list);
	}

	public function getMessagesByPartner($memberid,$supplierid){

		$list = array();
		$sql = "
			CALL find_messagesByPartner($memberid,$supplierid)
		";

		$res = $this->sql_obj->CALL($sql);

		while ($row = mysqli_fetch_array($res)){
			$return['id'] = $row['id'];
			$return['memberid'] = $row['memberid'];
			$return['supplierid'] = $row['supplierid'];
			$return['sender'] = $row['sender'];
			$return['content'] = $row['content'];
			$return['jobid'] = $row['jobid'];
			$return['timestamp'] = $row ['timestamp'];
			$list[] = $return;
		}

		echo json_encode($list);
	}


	public function sendMessage($memberid,$supplierid,$sender,$content,$jobid){

		$sql ="
			CALL send_message($memberid,$supplierid,$sender,\"$content\",$jobid)
		";

		if(!$this->sql_obj->CALL($sql)){
			$return['Status'] = "Failed";
		} else {
			$return['Status'] = "Success";
	    }
		echo json_encode($return);
	}

	public function deleteMessage($messId){

		$sql = "
			CALL deleteMessageById($messId)
		";

		if(!$this->sql_obj->CALL($sql)){
			$return['Status'] = "Failed";
		} else {
			$return['Status'] = "Success";
	    }

		echo json_encode($return);
	    
	}
}




