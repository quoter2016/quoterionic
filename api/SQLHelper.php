<?php

class SQLHelper {

	private $isConnected = false;

	private $user;
	private $pass;
	private $host;
	private $dbName;
	private $conn;
	private $sql;

	private $TBL_MEMBER = "qtr_member";
	private $TBL_ACCESS = "qtr_access";
	
	protected static $instance = null;

	public static function get_instance() {

		if(SQLHelper::$instance == null)
			SQLHelper::$instance = new SQLHelper();
		return SQLHelper::$instance;
	}

	protected function __construct() {
		// $this->user = "root";
		// $this->pass = "";
		// $this->host = "localhost";
		// $this->dbName = "gfunds_gruntfunds";

		// Quoter DB
		$this->user = "root";
		$this->pass = "5grWhQKj";
		$this->host = "localhost";
		$this->dbName = "quoter_db";

		// live
		// $this->user = "mmoyer_pslicer";
		// $this->pass = "34*7;NFcF#pz";
		// $this->host = "localhost";
		// $this->dbName = "mmoyer_pieslicer";
		
		$this->connect();
		// $this->init();
	}

	private function connect() {
		$this->conn = mysqli_connect($this->host, $this->user, $this->pass, $this->dbName);
	}

	public function CALL($query, $returnArray = false) {
		if(!$this->isConnected) $this->connect();
		if($res = $this->conn->query($query)) {
			if(!$returnArray) {
				return $res;
			}
			$rows = array();
			while ($temp = mysqli_fetch_array($res)) {
				$rows[] = $temp;
			}
			mysqli_free_result($res);
			return $rows;
		}
		die(mysqli_error($this->conn));
		return false;
	}

	public function SELECT($query, $returnArray = false){
		if(!$this->isConnected) $this->connect();
		if($res = $this->conn->query($query)) {
			if(!$returnArray)
				return $res;

			$rows = array();
			while ($temp = mysqli_fetch_array($res)) {
				$rows[] = $temp;
			}
			mysqli_free_result($res);
			return $rows;
		}
		die(mysqli_error($this->conn));
		return false;
	}

	public function INSERT($query){
		if($this->conn->query($query)) return true;
		die(mysqli_error($this->conn));
		return false;
	}

	public function query($query){
		if($this->conn->query($query)) return true;
		die(mysqli_error($this->conn));
		return false;
	}

	public function close() {
		$this->conn->close();
	}
}