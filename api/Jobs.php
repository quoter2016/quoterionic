<?php
require_once('SQLHelper.php');

class Jobs {

	private $sql_obj = null;

	public function __construct(){

		$this->sql_obj = SQLHelper::get_instance();
	}

	public function getJobsBySupplierId($searchId){
		$list = array();
		$rows = array();
		$sql = "
			CALL find_jobsBySupplierId(
				'$searchId')
		";

		$rows = $this->sql_obj->CALL($sql,true);


		foreach($rows as $row){
			// print_r($row);
			//GET JOB TYPES
			$this->sql_obj->close();
			$sql="
				SELECT *
				FROM qtr_jobtypes
				WHERE id = '".$row['jobtypeid']."'
			";
			$result = $this->sql_obj->SELECT($sql);
			$result= (array) mysqli_fetch_array($result);
			$jobtype['id'] = $result['id'];
			$jobtype['description'] = $result['description'];

			//GET MEMBER
			$sql="
				SELECT *
				FROM qtr_users
				WHERE id = '".$row['memberid']."'
			";
			$result6 = $this->sql_obj->SELECT($sql);
			$result6= (array) mysqli_fetch_array($result6);
			$member['id'] = $result6['id'];
			$member['email'] = $result6['email'];
			$member['first_name'] = $result6['first_name'];
			$member['last_name'] = $result6['last_name'];
			// print_r($result6);


			//GET SUPPLIER
			$sql="
				SELECT *
				FROM qtr_supplier
				WHERE userid = '".$row['supplierid']."'
			";
			$result1 = $this->sql_obj->SELECT($sql);
			$result1= (array) mysqli_fetch_array($result1);
			$supplier['id'] = $row['supplierid'];
			$supplier['email'] = $result6['email'];
			$supplier['first_name'] = $result6['first_name'];
			$supplier['last_name'] = $result6['last_name'];
			$spoint['lat'] = $result1['lat'];
			$spoint['lng'] = $result1['lng'];
			$supplier['location'] = $spoint;
			

			//GET LABOURTYPE
			$sql="
				SELECT *
				FROM qtr_labourtype
				WHERE id = '".$row['labourtypeid']."'
			";
			$result2 = $this->sql_obj->SELECT($sql);
			$result2= (array) mysqli_fetch_array($result2);
			$labourtype['id'] = $result2['id'];
			$labourtype['description'] = $result2['description'];
			
			//GET BECAUSE
			$sql="
				SELECT *
				FROM qtr_because
				WHERE id = '".$row['because']."'
			";
			$result3= $this->sql_obj->SELECT($sql);
			$result3= (array) mysqli_fetch_array($result3);
			$because['id'] = $result3['id'];
			$because['description'] = $result3['description'];
			
			//GET SIZE
			$sql="
				SELECT *
				FROM qtr_size
				WHERE id = '".$row['because']."'
			";
			$result4 = $this->sql_obj->SELECT($sql);
			$result4= (array) mysqli_fetch_array($result4);
			$size['id'] = $result4['id'];
			$size['description'] = $result4['description']; 
			
			//GET START
			$sql="
				SELECT *
				FROM qtr_start
				WHERE id = '".$row['start']."'
			";
			$result5 = $this->sql_obj->SELECT($sql);
			$result5= (array) mysqli_fetch_array($result5);
			$start['id'] = $result5['id'];
			$start['description'] = $result5['description'];
			


			$return['id'] = $row['id'];
			$return['jobtype'] = $jobtype;
			$return['supplier'] = $supplier;
			$return['member'] = $member;
			$return['jobname'] = $row['jobname'];
			$return['description'] = $row['description'];
			$return['labourtypeid'] = $labourtype;
			$point['lat'] = $row['lat'];
			$point['lng'] = $row['lng'];
			$return['location'] = $point;
			$return['because'] = $because;
			$return['size'] = $size;
			$return['start'] = $start;
			$list[] = $return;
		}
		echo json_encode($list);
	}

	public function findNearestJobs($lat,$lng,$lim,$cat){
		$sql = "
		CALL find_nearest_jobs($lat,$lng,$lim,100,$cat)
		";
		$list = array();
		$res = $this->sql_obj->CALL($sql);

		while ($row = mysqli_fetch_array($res)) {
			//GET JOB TYPES
			$this->sql_obj->close();
			$sql="
				SELECT *
				FROM qtr_jobtypes
				WHERE id = '".$row['jobtypeid']."'
			";
			$result = $this->sql_obj->SELECT($sql);
			$result= (array) mysqli_fetch_array($result);
			$jobtype['id'] = $result['id'];
			$jobtype['description'] = $result['description'];

			//GET MEMBER
			$sql="
				SELECT *
				FROM qtr_users
				WHERE id = '".$row['memberid']."'
			";
			$result6 = $this->sql_obj->SELECT($sql);
			$result6= (array) mysqli_fetch_array($result6);
			$member['id'] = $result6['id'];
			$member['email'] = $result6['email'];
			$member['first_name'] = $result6['first_name'];
			$member['last_name'] = $result6['last_name'];
			// print_r($result6);

			

			//GET LABOURTYPE
			$sql="
				SELECT *
				FROM qtr_labourtype
				WHERE id = '".$row['labourtypeid']."'
			";
			$result2 = $this->sql_obj->SELECT($sql);
			$result2= (array) mysqli_fetch_array($result2);
			$labourtype['id'] = $result2['id'];
			$labourtype['description'] = $result2['description'];
			
			//GET BECAUSE
			$sql="
				SELECT *
				FROM qtr_because
				WHERE id = '".$row['because']."'
			";
			$result3= $this->sql_obj->SELECT($sql);
			$result3= (array) mysqli_fetch_array($result3);
			$because['id'] = $result3['id'];
			$because['description'] = $result3['description'];
			
			//GET SIZE
			$sql="
				SELECT *
				FROM qtr_size
				WHERE id = '".$row['because']."'
			";
			$result4 = $this->sql_obj->SELECT($sql);
			$result4= (array) mysqli_fetch_array($result4);
			$size['id'] = $result4['id'];
			$size['description'] = $result4['description']; 
			
			//GET START
			$sql="
				SELECT *
				FROM qtr_start
				WHERE id = '".$row['start']."'
			";
			$result5 = $this->sql_obj->SELECT($sql);
			$result5= (array) mysqli_fetch_array($result5);
			$start['id'] = $result5['id'];
			$start['description'] = $result5['description'];

			// $supplier['id'] = $row['id'];
			// $supplier['jobtypeid'] = $row['jobtypeid'];
			// $supplier['memberid'] = $row['memberid'];
			// $supplier['supplierid'] = $row['supplierid'];
			// $supplier['jobname'] = $row['jobname'];

			// $supplier['description'] = $row['description'];
			// $supplier['labourtypeid'] = $row['labourtypeid'];

			// $point['lat'] = $row['lat'];
			// $point['lng'] = $row['lng'];
			// $supplier['location'] = $point; 
			// $supplier['because'] = $row['because'];
			// $supplier['size'] = $row['size'];
			// $supplier['start'] = $row['start'];
			// $supplier['distance'] = $row['distance'];
			// $list[] = $supplier;
			$return['id'] = $row['id'];
			$return['jobtype'] = $jobtype;
			$return['member'] = $member;
			$return['jobname'] = $row['jobname'];
			$return['description'] = $row['description'];
			$return['labourtypeid'] = $labourtype;
			$point['lat'] = $row['lat'];
			$point['lng'] = $row['lng'];
			$return['location'] = $point;
			$return['because'] = $because;
			$return['size'] = $size;
			$return['start'] = $start;
			$return['timestamp'] = $row['timestamp'];
			$list[] = $return;
		}

		echo json_encode($list);
	}

	public function deleteJobsById($jobid){
		$sql = "
			CALL deleteJobsById($jobid)
		";

		$res = $this->sql_obj->CALL($sql);

		if(!$res){
			$return['status'] = "FAILED";
		} else {
			$return['status'] = "SUCCESS";
		}

		echo json_encode($return);
	}
}