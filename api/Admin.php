<?php 

require_once('SQLHelper.php');

class Admin {

	private $sql_obj = null;

	public function __construct(){

		$this->sql_obj = SQLHelper::get_instance();
	}

	public function findMemberById($member_id){
		$sql = "
		CALL find_memberById($member_id)
		";
		$return= array();
		$res = $this->sql_obj->CALL($sql);

		if($res->num_rows > 0){
			while($row = (array) mysqli_fetch_array($res)){
				$return['id'] = $row['id'];
				$return['username'] = $row['username'];
				$return['password'] = $row['password'];
				$return['email'] = $row['email'];
				$return['first_name'] = $row['first_name'];
				$return['last_name'] = $row['last_name'];
				$return['timestamp'] = $row['timestamp'];
			}
		}

		echo json_encode($return);
	}

		public function getusercount(){

			$return = array();

		$sql = "SELECT count(*) as count FROM `qtr_users`";
		$res = $this->sql_obj->SELECT($sql);	

		if($res->num_rows > 0){
			while($row = (array) mysqli_fetch_array($res)){
				$return['count'] = $row['count'];
			}
		}

//			$return["count"] = $count;

		echo json_encode($return);
		}

		public function gettime(){

			$return = array();

//			$d = date("Y-m-d");
			$d = date("F-d-y"); //lower case d is 2 digit day

			$return["result"] = $d;

		echo json_encode($return);
		}

		public function gethash($fname,$lname){
			$return = array();

			$fullname = $fname + " " + $lname;
			$return["result"] = md5($fullname);

		echo json_encode($return);
		}

		public function calc($a,$b){
			$return = array();

			$c = $a + $b;
			$return["Answer"] = $c;

		echo json_encode($return);
		}


	public function findMemberByName($searchKey){
		$sql = "
		CALL find_memberByName('$searchKey')
		";
		$list = array();
		$return = array();
		$res = $this->sql_obj->CALL($sql);

		while($row = (array) mysqli_fetch_array($res)){
				$return['id'] = $row['id'];
				$return['username'] = $row['username'];
				$return['password'] = $row['password'];
				$return['email'] = $row['email'];
				$return['first_name'] = $row['first_name'];
				$return['last_name'] = $row['last_name'];
				$return['timestamp'] = $row['timestamp'];
				$list[] = $return;
		}

		echo json_encode($list);
	}

	public function findAccesstoken($access_token){
		$return = array();
		$user1 = array();

		$sql = "
		CALL find_access_token('$access_token')
		";

		$res = $this->sql_obj->CAll($sql);

		$row = mysqli_fetch_array($res);
		$return['id'] = $row['id'];
		$return['access_token'] = $row['Access_Token'];
		// $row = mysqli_fetch_array($res);
				$user1['id'] = $row['User_Id'];
				$user1['username'] = $row['Username'];
				$user1['password'] = $row['Password'];
				$user1['email'] = $row['Email'];
				$user1['first_name'] = $row['First_Name'];
				$user1['last_name'] = $row['Last_Name'];
				$user1['timestamp'] = $row['User_Timestamp'];
		$return['user'] = $user1;
		$return['timestamp'] = $row['Access_Timestamp'];
		$return['keep_signed_in'] = $row['Keep_Signed_In'];
		$return['expire'] = $row['Expire'];

		echo json_encode($return);
	}

	// ADD FUNCTIONS

	public function addBecause($description_bec){

		$description['because'] = $description_bec;

		$sql = "
		INSERT INTO 
		qtr_because (description) 
		VALUES ('".$description['because']."')"; 

		$res = $this->sql_obj->INSERT($sql);

		if(!$res){
			$return['Status'] = "FAILED";
		} else {
			$return['Status'] = "SUCCESS";
		}

		echo json_encode($return);
	}

	public function addJobtype($description_jobty){

		$description['jobtypes'] = $description_jobty;

		$sql = "
		INSERT INTO 
		qtr_jobtypes (description) 
		VALUES ('".$description['jobtypes']."')"; 

		$res = $this->sql_obj->INSERT($sql);

		if(!$res){
			$return['Status'] = "FAILED";
		} else {
			$return['Status'] = "SUCCESS";
		}

		echo json_encode($return);
	}

	public function addStart($description_start){

		$description['start'] = $description_start;

		$sql = "
		INSERT INTO 
		qtr_start (description) 
		VALUES ('".$description['start']."')"; 

		$res = $this->sql_obj->INSERT($sql);

		if(!$res){
			$return['Status'] = "FAILED";
		} else {
			$return['Status'] = "SUCCESS";
		}

		echo json_encode($return);
	}

	public function addSize($description_size){

		$description['size'] = $description_size;

		$sql = "
		INSERT INTO 
		qtr_size (description) 
		VALUES ('".$description['size']."')"; 

		$res = $this->sql_obj->INSERT($sql);

		if(!$res){
			$return['Status'] = "FAILED";
		} else {
			$return['Status'] = "SUCCESS";
		}

		echo json_encode($return);
	}

 // GET ALL FUNCTIONS

	public function getAllBecause(){

		$list = array();

		$sql = "SELECT * FROM qtr_because";
		$res = $this->sql_obj->SELECT($sql);	

	    if ($res->num_rows > 0) {
		
           while($user = (array) mysqli_fetch_array($res)){
           		$return = array();
           		$return['id'] = $user['id'];
           		$return['description'] = $user['description'];
           		$list[] = $return;
            }
          } else {
			$list['Status'] = "Failed";
        }

        echo json_encode($list);
	}

	public function getAllJobtypes($parent=0){

		$list = array();
		$sql = "SELECT * FROM qtr_jobtypes
				WHERE parent = $parent";
		$res = $this->sql_obj->SELECT($sql);	

	    if ($res->num_rows > 0) {
	
           while($user = (array) mysqli_fetch_array($res)) {
              $return = array();
           		$return['id'] = $user['id'];
           		$return['description'] = $user['description'];
           		$return['parent'] = $user['parent'];
           		$list[] = $return;
            }
          } else {
			$list['Status'] = "Failed";
        }
        echo json_encode($list);
	}

	public function getAllStart(){

		$list = array();
		$sql = "SELECT * FROM qtr_start";
		$res = $this->sql_obj->SELECT($sql);	

	    if ($res->num_rows > 0) {
	
           while($user = (array) mysqli_fetch_array($res)) {
             	$return = array();
           		$return['id'] = $user['id'];
           		$return['description'] = $user['description'];
           		$list[] = $return;
            }
          } else {
			$list['Status'] = "Failed";
        }
        echo json_encode($list);
	}

	public function getAllSize(){

		$list = array();
		$sql = "SELECT * FROM qtr_size";
		$res = $this->sql_obj->SELECT($sql);	

	    if ($res->num_rows > 0) {
	
           while($user = (array) mysqli_fetch_array($res)) {
                $return = array();
           		$return['id'] = $user['id'];
           		$return['description'] = $user['description'];
           		$list[] = $return;
            }
          } else {
			$list['Status'] = "Failed";
        }
        echo json_encode($list);
	}

}




