<?php

class Routes {

	public static function executeRoute($route) {
		$modulePath = ucwords($route["module"]).".php";
		$module = strtolower($route["module"]);
		$action = strtolower($route["action"]);

		if(!file_exists($modulePath)){
			handleError();
		}

		require_once($modulePath);
		Routes::executeAction($module, $action);
	}

	protected static function executeAction($module, $action) {
		$endpoint = "/".$module."/".$action;
		if($module == "user"){
			$action = new User();
		} else if($module == "supplier") {
			$action = new Supplier();
		} else if($module == "admin"){
			$action = new Admin();
		} else if($module == "messages"){
			$action = new Messages();
		} else if($module == "jobs"){
			$action = new Jobs();
		} else if($module == "facebook"){
			$action = new Facebook();
		} else if($module == "social"){
			$action = new Social();
		} else {
			$action = new Preferences();
		}
		
		switch ($endpoint) {

			/*
			** THIS CONSIST OF ROUTES FROM ENDPOINT URL
			** DO NOT ENTER COSTLY PROCESSES
			** THIS WILL ONLY CALL CORRESPONDING METHODS IN CLASS
			** OR PARSING $_POST / $_GET / $_REQUEST PARAMETERS OR ISSET VALIDATION
			** FOR CLASS INPUT PARAMETERS
			*/

			case '/user/wayne':
					$action->wayne();
				break;

			case '/user/test_getuserbyemaildomain':
					$action->test_getUserByEmailDomain($_POST['domain']);
				break;

			case '/user/testuid':
					$action->testuid($_POST['uid']);
				break;

			case '/admin/calc':
					$action->calc($_POST['a'],$_POST['b']);
				break;

			case '/admin/getusercount':
					$action->getusercount();
				break;

			case '/admin/gettime':
					$action->gettime();
				break;

			case '/admin/gethash':
					$action->gethash($_POST['fname'],$_POST['lname']);
				break;

			case '/user/login':
				if(isset($_POST["username"])) {
					$action->login($_POST['username'],$_POST['password'],$_POST['rememberme']);
					// $action->login("zxc","cxz",0);

				} else
					Routes::handleError();
				break;
			
			case '/user/register':
					$user_obj = array();
					$user_obj['username'] = $_POST['username'];
					$user_obj['password'] = $_POST['password'];
					$user_obj['email'] = $_POST['email'];
					$user_obj['first_name'] = $_POST['first_name'];
					$user_obj['last_name'] = $_POST['last_name'];

					$action->register($user_obj);
				break;
			case '/user/getjobsbyuserid':
				if($_POST['id']) 
					$action->getJobsByUserId($_POST['id']);
				else
					Routes::handleError();
				break;
			case '/user/postjob':
					$action->addJob($_POST['jobtypeid'],$_POST['memberid'],$_POST['jobname'],$_POST['description'],$_POST['labourtypeid'],$_POST['lat'],$_POST['lng'],$_POST['because'],$_POST['size'],$_POST['start']);
				break;
			case '/user/findnearestsupplier':
					$action->findNearestSupplier($_POST['lat'],$_POST['lng'],$_POST['lim'],$_POST['cat']);
				break;
			case '/user/bookbysupplierid':
					$action->bookBySupplierId($_POST['jobid'],$_POST['supplierid']);
				break;
			case '/supplier/login':
				if(isset($_POST["username"])) {
					$action->login($_POST['username'],$_POST['password'],$_POST['rememberme']);
					// $action->login("lol","lol",1);
				} else
					Routes::handleError();
				break;
			case '/supplier/register':
					$user_obj = array();
					$user_obj['username'] = $_POST['username'];
					$user_obj['password'] = $_POST['password'];
					$user_obj['email'] = $_POST['email'];
					$user_obj['first_name'] = $_POST['first_name'];
					$user_obj['last_name'] = $_POST['last_name'];
					$user_obj['lat'] = $_POST['lat'];
					$user_obj['lng'] = $_POST['lng'];
					$user_obj['categoryid'] = $_POST['categoryid'];

					$action->register($user_obj);
				break;
			case '/supplier/findnearestjobs':
					$action->findNearestJobs($_POST['lat'],$_POST['lng'],$_POST['lim'],$_POST['cat']);
				break;
			case '/supplier/getjobsbysupplierid':
					$action->getJobsBySupplierId($_POST['id']);
				break;
			case '/admin/getalljobtypes':
					$action->getAllJobtypes($_POST['parent']);
				break;
			case '/messages/getmessagesbyjobid':
					$action->getMessagesByJobId($_POST['jobid']);
				break;
			case '/messages/getmessagesbymemberid':
					$action->getMessagesByMemberId($_POST['memberid']);
				break;
			case '/messages/getmessagesbysupplierid':
					$action->getMessagesBySupplierid($_POST['supplierid']);
				break;
			case '/messages/getmessagesbypartner';
					$action->getMessagesByPartner($_POST['memberid'],$_POST['supplierid']);	
				break;
			case '/messages/sendmessage':
					$action->sendMessage($_POST['memberid'],$_POST['supplierid'],$_POST['sender'],$_POST['content'],$_POST['jobid']);
				//echo json_encode(($return['sendmessage'] = true));
				break;
			case '/messages/deletemessage':
					$action->deleteMessage($_POST['id']);
				break;
			case '/preferences/changepassword':
					$action->changePassword($_POST['id'],$_POST['password']);
				break;
			case '/preferences/changelocation':
					$action->changeLocation($_POST['id'],$_POST['lat'],$_POST['lng']);
				break;
			case '/preferences/isexpire':
					$action->isExpire($_POST['id']);
				break;
			case '/preferences/logout':
					$action->logout($_POST['id']);
				break;
			case '/jobs/deletejob':
					$action->deleteJobsById($_POST['id']);
				break;
			case '/social/supplierlogin':
					$user_obj = array();
					$user_obj['username'] = "";
					$user_obj['password'] = "";
					$user_obj['access_token'] = $_POST['access_token'];
					$user_obj['email'] = $_POST['email'];
					$user_obj['first_name'] = $_POST['first_name'];
					$user_obj['last_name'] = $_POST['last_name'];
					$user_obj['lat'] = $_POST['lat'];
					$user_obj['lng'] = $_POST['lng'];
					$user_obj['categoryid'] = $_POST['categoryid'];
					$user_obj['login_type'] = $_POST['login_type'];

					$action->supplierLogin($user_obj);
				break;
			case '/facebook/addaccess':
					$user_obj = array();
					$user_obj['username'] = "";
					$user_obj['password'] = "";
					$user_obj['access_token'] = $_POST['access_token'];
					$user_obj['email'] = $_POST['email'];
					$user_obj['first_name'] = $_POST['first_name'];
					$user_obj['last_name'] = $_POST['last_name'];
					$action->facebookAddAccess($user_obj);
				break;
			case '/facebook/register':
					$user_obj = array();
					$user_obj['username'] = "";
					$user_obj['password'] = "";
					$user_obj['access_token'] = $_POST['access_token'];
					$user_obj['email'] = $_POST['email'];
					$user_obj['first_name'] = $_POST['first_name'];
					$user_obj['last_name'] = $_POST['last_name'];
					$action->facebookRegister($user_obj);
				break;
			case '/social/memberlogin':
					$user_obj = array();
					$user_obj['username'] = "";
					$user_obj['password'] = "";
					$user_obj['access_token'] = $_POST['access_token'];
					$user_obj['email'] = $_POST['email'];
					$user_obj['first_name'] = $_POST['first_name'];
					$user_obj['last_name'] = $_POST['last_name'];
					$user_obj['login_type'] = $_POST['login_type'];
					$action->memberLogin($user_obj);
				break;	
			default:
				Routes::handleError();
				break;
		}
	}

	public static function handleError() {
		http_response_code(404);
		$response = array();
		$response["error"] = "Resource Not Found";
		die(json_encode($response));
	}
}