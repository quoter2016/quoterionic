angular.module('starter.controllers')
.controller('PostJobCtrl', function($scope,$window, $stateParams,$interval,$location, $http, $timeout, $ionicPopup,$ionicLoading, PostJob, LocationData) {
    $scope.postJobData = {};
    $scope.categories = PostJob.all();
    $scope.jobtypes = PostJob.allJobtypes();

    // $scope.x1 = LocationData.all().lat;
    // $scope.x2 = LocationData.all().lng;

    $(document).ready(function() {
        var text_max = 1000;
        $('#textarea_feedback').html(text_max + ' characters remaining');
        $('#textAreaDescription').keyup(function() {
            var text_length = $('#textAreaDescription').val().length;
            var text_remaining = text_max - text_length;

            $('#textarea_feedback').html(text_remaining + ' characters remaining');
        });


    }); 

    $(document).on('change', '#jobCategory', function() {
        var index = document.getElementById('jobCategory').selectedIndex+1;
        console.log(index);
        $ionicLoading.show({
          template: 'Loading Job Types...'
        })
        $scope.loadCategories();
    });

    $ionicLoading.show({
      template: 'Loading Job Categories...'
    })
    var linkGetCat = "parent=0";
    $http.post($scope.domainLink+"/admin/getalljobtypes", linkGetCat).then(function(res) {
        PostJob.setCategories(res.data);
        // $timeout(function() {
        $scope.categories = PostJob.all();
        console.log($scope.categories);
        // }, 2000);
    //$scope.loadCategories();
    })
    .finally(function(){
        $ionicLoading.hide();
    });


    //initial types
    $ionicLoading.show({
          template: 'Loading Job Types...'
        })
        var linkGetJobTypes = "parent=1";
        $http.post($scope.domainLink+"/admin/getalljobtypes", linkGetJobTypes).then(function(res) {
            PostJob.setJobTypes(res.data);

            //$timeout(function() {
                $scope.jobtypes = PostJob.allJobtypes();
            //}, 500);
        })
        .finally(function(){
            $ionicLoading.hide();
        });
    //end initial jobtypes

    $scope.loadCategories = function() {
        $ionicLoading.show({
          template: 'Loading Job Types...'
        })
        var index = document.getElementById('jobCategory').selectedIndex+1;
        var linkGetJobTypes = "parent="+index;
        $http.post($scope.domainLink+"/admin/getalljobtypes", linkGetJobTypes).then(function(res) {
            PostJob.setJobTypes(res.data);

            //$timeout(function() {
                $scope.jobtypes = PostJob.allJobtypes();
            //}, 500);
        })
        .finally(function(){
            $ionicLoading.hide();
        });
    };
    // console.log(document.getElementById('estimateTypes').selectedIndex);

    $scope.postjob = function(data) {
        $ionicLoading.show({
          template: 'Posting Job...'
        })
        data.jobCategory = document.getElementById('jobCategory').selectedIndex;
        data.jobType = document.getElementById('jobType').selectedIndex;

        data.because = document.getElementById('because').selectedIndex;
        data.sizee = document.getElementById('sizee').selectedIndex;
        data.startt = document.getElementById('startt').selectedIndex;
        data.estimateTypes = document.getElementById('estimateTypes').selectedIndex;
        data.lat = $("#latIn").val();
        data.llong = $("#longIn").val();
        console.log(data.jobCategory);
        console.log(data.jobType);

        var indexString = document.getElementById('jobType').value;
        data.jobTypeId = PostJob.getJobTypeId(indexString);
        // console.log(indexString);
        // console.log(jobTypeId);

        if (data.estimateTypes==0) {
            // data.labourMaterials = 1;
            // data.laborOnly = 0;
            data.labourmat = 1;
        } else {
            // data.labourMaterials = 0;
            // data.laborOnly = 1;
            data.labourmat = 2;
        }
        //FORM HANDLING. WALA SA NAKA FUNCTION HEGS
        var formErrors = "";
        if(data.jobname==null){
            formErrors = formErrors + "- Please provide a Job Name<br>";
        }

        if(data.jobname!=null){
            if(data.jobname.length<5){
                formErrors = formErrors + "- Please provide a valid Job Name<br>";
            }
        }

        if(data.jobType==-1){
            formErrors = formErrors + "- Please select a Job Category<br>";
        }
        if(data.jobType==-1){
            formErrors = formErrors + "- Please select a Job Type<br>";
        }


        if(data.jobDescription==null){
            formErrors = formErrors + "- Please provide a Description<br>";
        }

        if(data.jobDescription!=null){
            if(data.jobDescription.length<5){
                formErrors = formErrors + "- Please enter a Detailed Description of the work<br>";
            }
        }
        if(isNaN(data.lat) || data.lat==null){
            formErrors = formErrors + "- Please enter a valid Latitude<br>";
        }
        if(isNaN(data.llong) || data.lat==null){
            formErrors = formErrors + "- Please enter a valid Longitude<br>";
        }
        if(data.because == 0){
            formErrors = formErrors + "- Please indicate why you want the quote<br>";
        }
        if(data.sizee == 0){
            formErrors = formErrors + "- Please indicate the size of the work<br>";
        }
        if(data.startt == 0){
            formErrors = formErrors + "- Please indicate when you want the work to start<br>";
        }
        if (formErrors.length>0) {
            console.log(formErrors);
            $ionicLoading.hide();
            $ionicPopup.alert({
                title: 'Error on input',
                template: formErrors
            });
        } else if(formErrors.length==0){

        //console.log(data);
        //xs
        var linkPostJob = "jobtypeid="+data.jobTypeId+"&memberid="+$scope.loginData.userid+"&jobname="+data.jobname+"&description="+data.jobDescription+"&labourtypeid="+data.labourmat+"&lat="+data.lat+"&lng="+data.llong+"&because="+data.because+"&size="+data.sizee+"&start="+data.startt+"";
            $http.post($scope.domainLink+"/user/postjob", linkPostJob).then(function(res) {
            console.log(res.data);

            if(res.data.Status=="SUCCESS"){
                var postedPop = $ionicPopup.alert({
                title: 'Information',
                template: 'Job Posted Succesfully!'
            });
            postedPop.then(function(res) {
                if(res) {
                    //$window.location.reload();
                    $location.path('/app/member-menu');
                } else {
                    //console.log('dnt delete');

                }
            });
            }

        })
        .finally(function(){
            $ionicLoading.hide();

        });

        }


    };

    $scope.Timer = null;
    //Timer start function.
    $scope.StartTimer = function() {
        $scope.Timer = $interval(function() {
            $scope.x1 = LocationData.all().lat;
            $scope.x2 = LocationData.all().lng;
        }, 1000);
    };

    //Timer stop function.
    $scope.StopTimer = function() {

        //Set the Timer stop message.
        // $scope.Message = "Timer stopped.";

        //Cancel the Timer.
        if (angular.isDefined($scope.Timer)) {
            $interval.cancel($scope.Timer);
        }
    };

    $scope.$on("$destroy", function() {
        $scope.StopTimer();
    });
});