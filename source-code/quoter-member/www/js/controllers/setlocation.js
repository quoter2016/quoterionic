angular.module('starter.controllers')
.controller('SetLocationCtrl', function($scope,$location,$timeout,$ionicPopup,$ionicLoading, $stateParams,LocationData) {
	var geocoder;
	var map;
	var currdataLoc = [];
	var marker;

	map = new google.maps.Map(document.getElementById("mapForSL"),
	{
	    zoom: 16,
	    mapTypeId: google.maps.MapTypeId.ROADMAP
	});

	google.maps.event.addListener(map, 'dblclick', function(event) {
	    // console.log(event.latLng.lat());
	    // console.log(event.latLng.lng());
	    currdataLoc.lat = event.latLng.lat();
	    currdataLoc.lng = event.latLng.lng();

	    LocationData.setLoc(currdataLoc);
	    
	        $scope.x1 = LocationData.all().lat;
	        $scope.x2 = LocationData.all().lng;

	    marker.setPosition(new google.maps.LatLng($scope.x1,$scope.x2));

	    $ionicLoading.show({
	      template: 'Please wait..'
	    })

	    $timeout(function() {
	        $scope.x1 = LocationData.all().lat;
	        console.log("Lat: "+ $scope.x1 );
	            }, 1000);
	    $timeout(function() {
	        $scope.x2 = LocationData.all().lng;
	        console.log("Long: "+ $scope.x2);
	        $ionicLoading.hide();
	            }, 1000);


	});

	$scope.loading = $ionicLoading.show({
	      showBackdrop: true
	    });

	navigator.geolocation.getCurrentPosition(function(pos) {
      map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
          currdataLoc.lat = pos.coords.latitude;
          currdataLoc.lng = pos.coords.longitude;
            LocationData.setLoc(currdataLoc);
            $scope.x1 = LocationData.all().lat;
            $scope.x2 = LocationData.all().lng;
            // console.log("CURRENT LOC");
            //console.log(currdataLoc);
            marker = new google.maps.Marker({
                        map: map,
                        position: new google.maps.LatLng($scope.x1,$scope.x2),
                        title: "You are here"
                    });
      $ionicLoading.hide();
    }, 
    function(error) {
      $ionicPopup.alert({
                title: 'Error',
                template: "Unable to get current position for the following reason: "+error
            });
    });

	$scope.addressInput = "";

 	$scope.centerMap = function(){

     geocoder = new google.maps.Geocoder();

    $scope.addressInput = $('#aLoc').val();
        geocoder.geocode( { 'address': $scope.addressInput}, function(results, status)
    {
        if (status == google.maps.GeocoderStatus.OK)
        {
            map.setCenter(results[0].geometry.location);


            $scope.latLoc = results[0].geometry.location.lat();
            $scope.longLoc = results[0].geometry.location.lng();

            currdataLoc.lat = $scope.latLoc;
            currdataLoc.lng = $scope.longLoc;
            LocationData.setLoc(currdataLoc);

            $scope.x1 = LocationData.all().lat;
            $scope.x2 = LocationData.all().lng;
            marker.setPosition(new google.maps.LatLng($scope.x1,$scope.x2));
    
        }
        else
        {
            //alert("Geocode was not successful for the following reason: " + status);
            $ionicPopup.alert({
                title: 'Error',
                template: "Geocode was not successful for the following reason: "+status+"<br><br>Try setting manual location points."
            });
        }
    });
 
 	$timeout(function() {
        console.log("Lat: "+$scope.latLoc);
        console.log("Long: "+$scope.longLoc);
            }, 1000);
	}

	$scope.confirmLoc = function(){
	    $ionicLoading.show({
	      template: 'Please wait..'
	    })
	    $timeout(function() {
	        $ionicLoading.hide();
	        $location.path('/app/post-job');
	    }, 1000);

	}
});