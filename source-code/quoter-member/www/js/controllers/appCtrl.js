angular.module('starter.controllers')
.controller('AppCtrl', function($scope, $ionicModal,$ionicLoading, $ionicPopover, $http, $timeout, $location, $ionicPopup) {


    $scope.domainLink = "http://149.56.98.5/api";
    $scope.loginData = {};
    $scope.supplierDetails = {};
    $scope.latLoc = "";
    $scope.longLoc = "";
    var currMsgid="";
    $scope.supplierDataMap = {};
    $scope.jobDataClicked = {};
    $http.defaults.headers.post["Content-Type"] = 'application/x-www-form-urlencoded;charset=utf-8;';
    $scope.tmpLat = "12.901";
    $scope.tmpLng = "100.900";

   if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(success);
    } else {
        console.log("Geolocation is not supported.");
        alert("Geolocation is not supported.");
    }
    function success(position) {
     $scope.tmpLat = position.coords.latitude;
     $scope.tmpLng = position.coords.longitude;
    }

    $scope.saved = localStorage.getItem('quoterAccessToken');
    $scope.quoterAccessToken = (localStorage.getItem('quoterAccessToken')!==null) ? JSON.parse($scope.saved) : [ {initial: 'ACCESS TOKEN'} ];
    localStorage.setItem('quoterAccessToken', JSON.stringify($scope.quoterAccessToken));

	$scope.logout = function() {
        // var index =0;
        // $scope.userDetailsStorage.splice(index, 1);
        // localStorage.removeItem(index);

        // localStorage.setItem('userDetailsStorage', JSON.stringify($scope.userDetailsStorage));

        // console.log($scope.userDetailsStorage);
        $location.path('/app/login');
    }
    //--------------------------------------------
    // An alert dialog
    $scope.showAlert = function(msg) {
        var alertPopup = $ionicPopup.alert({
            title: 'Warning Message',
            template: msg
        });
    }

});