angular.module('starter.controllers')
.controller('JobInformationCtrl', function($scope,$location,$timeout, $stateParams,$ionicLoading, $interval, $ionicPopup, $http) {
    $scope.jobInfo = $scope.jobDataClicked.data;
    console.log("JOB INFORMATION: ");
    console.log($scope.jobDataClicked.data);

    $("#jobDescription").val($scope.jobDataClicked.data.description);
    $("#jobType").val($scope.jobDataClicked.data.jobtype.description);

    $scope.deleteJob = function(jobid){
        var linkDelete="id="+jobid+"";
        var deleteJobSure = $ionicPopup.confirm({
            title: 'Delete Job',
            template: 'Are you sure to delete this job?',
            cancelText: 'Cancel',
            okText: 'Ok'
        });
        deleteJobSure.then(function(res) {
            if(res) {
                //console.log('Delete clicked');
                $ionicLoading.show({
                  template: 'Deleting Job...'
                })
            
            $http.post($scope.domainLink+"/jobs/deletejob", linkDelete).then(function(res) {
                    if(res.data.status=="SUCCESS"){
                        $ionicLoading.hide();
                        $ionicLoading.show({
                          template: 'Job deleted'
                        })
                        $timeout(function() {
                            $ionicLoading.hide();
                        }, 1000);
                        $location.path('/app/my-jobs');
                    } else {
                        $ionicLoading.hide();
                        $ionicLoading.show({
                          template: 'Error deleting job.'
                        })
                        $timeout(function() {
                            $ionicLoading.hide();
                        }, 1000);
                    }
                })
                .finally(function(){
                });
            } else {
                //console.log('dnt delete');

            }
        });
    }

});
