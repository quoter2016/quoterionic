angular.module('starter.controllers')
.controller('MyJobsCtrl', function($scope,$ionicLoading,$timeout, $stateParams, $interval, $ionicPopup, $http,$location, MyJobs) {
    $scope.jobs = [];
    $ionicLoading.show({
          template: 'Loading Jobs...'
        })
    $scope.getMyJobs = function() {
        var linkGetJobs = "id="+$scope.loginData.userid;
            $http.post($scope.domainLink+"/user/getjobsbyuserid", linkGetJobs).then(function(res) {
            $scope.response = res;
            var tmp1 = res.data;
            if($scope.response.data.error == true){
              $scope.showAlert(res.data.Message); 
            }else{
                // console.log("MY JOBS");
                // console.log($scope.jobs.length);
                //console.log(tmp1.length);
                if ($scope.jobs.length != tmp1.length) {
                    MyJobs.setJobs(tmp1);
                    //console.log(tmp1);
                    console.log("new job added");
                    $scope.jobs = MyJobs.all();
                }
            }
        }).finally(function(){
        $ionicLoading.hide();
    });
    };

    $scope.showJobInfo = function(jobData) {
        $scope.jobDataClicked.data = jobData; //to be used on JobInformationCtrl

        $location.path("/app/job-information");
    }

    $scope.Timer = null;
    //Timer start function.
    $scope.StartTimer = function() {
        $scope.Timer = $interval(function() {
            $scope.getMyJobs();
        }, 1000);
    };

    //Timer stop function.
    $scope.StopTimer = function() {

        //Set the Timer stop message.
        // $scope.Message = "Timer stopped.";

        //Cancel the Timer.
        if (angular.isDefined($scope.Timer)) {
            $interval.cancel($scope.Timer);
        }
    };

    $scope.$on("$destroy", function() {
        $scope.StopTimer();
    });

    $scope.deleteJobFromList = function(jobid){
        var linkDelete="id="+jobid+"";
        var deleteJobSure = $ionicPopup.confirm({
            title: 'Delete Job',
            template: 'Are you sure to delete this job?',
            cancelText: 'Cancel',
            okText: 'Ok'
        });
        deleteJobSure.then(function(res) {
            if(res) {
                //console.log('Delete clicked');
                $ionicLoading.show({
                  template: 'Deleting Job...'
                })
            
            $http.post($scope.domainLink+"/jobs/deletejob", linkDelete).then(function(res) {
                    if(res.data.status=="SUCCESS"){
                        $ionicLoading.hide();
                        $ionicLoading.show({
                          template: 'Job deleted'
                        })
                        $timeout(function() {
                            $ionicLoading.hide();
                        }, 1000);
                        $location.path('/app/my-jobs');
                    } else {
                        $ionicLoading.hide();
                        $ionicLoading.show({
                          template: 'Error deleting job.'
                        })
                        $timeout(function() {
                            $ionicLoading.hide();
                        }, 1000);
                    }
                })
                .finally(function(){
                });
            } else {
                //console.log('dnt delete');

            }
        });
    }
});