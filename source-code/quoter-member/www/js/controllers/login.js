angular.module('starter.controllers')
.controller('LoginCtrl', function($scope,$cordovaOauth, $ionicModal,$ionicLoading, $ionicPopover, $http, $timeout, $location, $ionicPopup) {

    $scope.keepSignin = function(accesstok, accessid) {
        // $scope.quoterAccessToken.push({
        //     token: accesstok,
        //     done: false
        // });
        $scope.quoterAccessToken = [{token: accesstok, userid: accessid}];
        localStorage.setItem('quoterAccessToken', JSON.stringify($scope.quoterAccessToken));
        console.log("TOKEN SAVED: ");
        console.log($scope.quoterAccessToken);
    };

    $scope.clearToken = function() {
        $scope.quoterAccessToken = [{initial: 'ACCESS TOKEN'}];
        localStorage.setItem('quoterAccessToken', JSON.stringify($scope.quoterAccessToken));
        console.log("TOKEN CLEARED: ");
        console.log($scope.quoterAccessToken);
    };

    console.log("TOKEN: ");
    console.log($scope.quoterAccessToken);

    if(typeof($scope.quoterAccessToken[0].token)!='undefined'){
        console.log($scope.quoterAccessToken[0].token);
    } else {
        //AUTO LOGIN
    }


    //--------------------------------------------
    $scope.login = function(user) {
        user.rememberme = 0;
        $(document).ready(function() {
                if ($('#stayIn').is(':checked')) {
                    user.rememberme = 1;
                }
        }); 

        if (typeof(user) == 'undefined') {
            $scope.showAlert('Please fill username and password to proceed.');
            return false;
        } else {
            $ionicLoading.show({
              template: 'Logging in...'
            })
            var linkLogin = "username="+user.username+"&password="+user.password+"&rememberme="+user.rememberme+"";
            $http.post($scope.domainLink+"/user/login", linkLogin).then(function (res){
            console.log(res.data);
            $scope.response = res;

            if($scope.response.data.error!=undefined){
              $scope.showAlert(res.data.status); 
            }else{
                $scope.loginData.accessToken = res.data.access_token;
                $scope.loginData.username = user.username;
                $scope.loginData.userid = res.data.user.id;
                //console.log("DATAAAAA "+$scope.loginData.userid);
                //console.log(res.data);

                if(user.rememberme==1){ 
                    $scope.keepSignin(res.data.access_token, res.data.user.id);
                } else {
                    $scope.clearToken();
                }
                $location.path('/app/member-menu');
            }
        })
        .finally(function(){
                $ionicLoading.hide();
            });
        }
    }

    $scope.googleLogin = function() {
        $cordovaOauth.google("536258911748-935ajmj6udftjgahp1namej20nvfpfd5.apps.googleusercontent.com", ["email"]).then(function(result) {
            console.log(JSON.stringify(result));
            var tokenGoogle = result.access_token;
            $scope.registerGoogle(tokenGoogle);

        }, function(error) {
            $ionicPopup.alert({
                 title: 'Error',
                 template: JSON.stringify(error)
               });
            console.log(error);
        });
    }
    $scope.facebookLogin = function (){
        $cordovaOauth.facebook("154214858324901", ["email"]).then(function(result){
              $scope.registerFacebook(result.access_token);
            console.log(JSON.stringify(result));
        }, function(error) {
            $ionicPopup.alert({
                 title: 'Facebook Error',
                 template: JSON.stringify(error)
               });
            //console.log(JSON.stringify(error));
        });
    }

    $scope.registerFacebook = function(access_token) {
      var tok = access_token;
      $ionicLoading.show({
              template: 'Please wait...'
            })
        $http.get("https://graph.facebook.com/v2.2/me", {params: {access_token: access_token, fields: "id,email,first_name, last_name", format: "json" }}).then(function(result) {
        var profile = result.data;
        // $ionicPopup.alert({
        //          title: '',
        //          template: "DATA RETURN FROM FB"+profile.id+"<br>"+profile.email+"<br>"+profile.first_name+"<br>"+profile.last_name
        //        });
        $scope.facebookToServer(profile,tok);
        }, function(error) {
            $ionicLoading.hide();
              $ionicPopup.alert({
                       title: 'Error',
                       template: JSON.stringify(error)
                     });
                alert("Error: " + error);
            }).finally(function(){
                $ionicLoading.hide();
        });

    }

    $scope.facebookToServer = function(dataFromFB,at){
        var data = dataFromFB;
        var access_token = at;

            $ionicLoading.show({
              template: 'Logging in...'
            })
            var linkLogin = "first_name="+data.first_name+"&last_name="+data.last_name+"&email="+data.email+"&access_token="+access_token+"&login_type="+"facebook"+"";
            $http.post($scope.domainLink+"/social/memberlogin", linkLogin).then(function (res){
                //$ionicLoading.hide();
                $scope.loginData.accessToken = res.data.access_token;
                $scope.loginData.username = res.data.user.email;
                $scope.loginData.userid = res.data.user.id;

            })
            .finally(function(){
                $ionicLoading.hide();
                $location.path('/app/member-menu');
                });
    }

    //========
    $scope.registerGoogle = function(access_token) {
        $ionicLoading.show({
              template: 'Please wait...'
            })
        $http({method:"GET", url:"https://www.googleapis.com/plus/v1/people/me?access_token="+access_token}).success(function(response){
                $scope.googleToServer(response,access_token);
              }, function(error) {
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                         title: '',
                         template: error
                   });
              console.log(error);
            }).finally(function(){
                $ionicLoading.hide();
            });

    }

    $scope.googleToServer = function(data,access_token){
            $ionicLoading.show({
              template: 'Logging in...'
            })

            var linkLogin = "first_name="+data.name.givenName+"&last_name="+data.name.familyName+"&email="+data.emails[0].value+"&access_token="+access_token+"&login_type="+"google"+"";
            $http.post($scope.domainLink+"/social/memberlogin", linkLogin).then(function (res){
                //$ionicLoading.hide();
                $scope.loginData.accessToken = res.data.access_token;
                $scope.loginData.username = res.data.user.email;
                $scope.loginData.userid = res.data.user.id;


        })
        .finally(function(){
                $ionicLoading.hide();
                $location.path('/app/member-menu');
            });
    }


});