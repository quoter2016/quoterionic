angular.module('starter.controllers')
.controller('PreferencesCtrl', function($scope, $stateParams,$ionicLoading, $interval, $ionicPopup, $http) {
    $scope.changePassword = function(data){

        if(typeof(data)!='undefined'){
            if(data.passNew==data.passNewRep){
                    $ionicLoading.show({
                      template: 'Changing Password...'
                    })
                    var linkLogin = "id="+$scope.loginData.userid+"&password="+data.passNew+"";
                    $http.post($scope.domainLink+"/preferences/changepassword", linkLogin).then(function (res){
                        console.log(res.data);
                        $scope.response = res;

                        if($scope.response.data.error!=undefined){
                          $scope.showAlert(res.data.status); 
                        }else{
                            //$location.path('/app/member-menu');
                        }
                        if(res.data.status=="SUCCESS"){
                            $ionicPopup.alert({
                                title: 'Success',
                                template: "Password changed successfully!"
                            }); 
                        }
                    })
                    .finally(function(){
                            $ionicLoading.hide();
                    });
            } else {
                $ionicPopup.alert({
                    title: 'ERROR',
                    template: "Password does not match."
                }); 
            }
        } else {
            $ionicPopup.alert({
                title: 'ERROR',
                template: "All fields are required."
            }); 
        }
    }
});