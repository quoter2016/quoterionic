angular.module('starter.controllers')
.controller('MapsCtrl', function($scope,$interval, $stateParams, $http, $timeout, $location, $ionicPopup, $ionicLoading, $compile, Maps, PostJob,MyJobs) {

    $scope.jobs=[];
    $scope.lat = 12.901;
    $scope.longg = 100.9;
    $scope.suppliers = Maps.allSuppliers();
    // $scope.suppliers = [
    // {'email':'ella@lol.com'}
    // ];
    $scope.postJobData = {};
    $scope.categories = PostJob.all();
    $scope.jobtypes = PostJob.allJobtypes();

    $(document).on('change', '#supplierJobs', function() {
        var index = document.getElementById('supplierJobs').selectedIndex+1;
        console.log(index);
        $ionicLoading.show({
          template: 'Loading Job Types...'
        })
        $scope.loadCategories();
    });

    $ionicLoading.show({
      template: 'Loading Job Categories...'
    })
    var linkGetCat = "parent=0";
    $http.post($scope.domainLink+"/admin/getalljobtypes", linkGetCat).then(function(res) {
        PostJob.setCategories(res.data);
        // $timeout(function() {
        $scope.categories = PostJob.all();
        console.log($scope.categories);
        // }, 2000);
    //$scope.loadCategories();
    })
    .finally(function(){
        $ionicLoading.hide();
    });


    //initial types
    $ionicLoading.show({
          template: 'Loading Job Types...'
        })
        var linkGetJobTypes = "parent=1";
        $http.post($scope.domainLink+"/admin/getalljobtypes", linkGetJobTypes).then(function(res) {
            PostJob.setJobTypes(res.data);

            //$timeout(function() {
                $scope.jobtypes = PostJob.allJobtypes();
            //}, 500);
        })
        .finally(function(){
            $ionicLoading.hide();
        });
    //end initial jobtypes

    $scope.loadCategories = function() {
        $ionicLoading.show({
          template: 'Loading Job Types...'
        })
        var index = document.getElementById('supplierJobs').selectedIndex+1;
        var linkGetJobTypes = "parent="+index;
        $http.post($scope.domainLink+"/admin/getalljobtypes", linkGetJobTypes).then(function(res) {
            PostJob.setJobTypes(res.data);

            //$timeout(function() {
                $scope.jobtypes = PostJob.allJobtypes();
            //}, 500);
        })
        .finally(function(){
            $ionicLoading.hide();
        });
    };



    $scope.getMyJobs = function() {
        var linkGetJobs = "id="+$scope.loginData.userid;
            $http.post($scope.domainLink+"/user/getjobsbyuserid", linkGetJobs).then(function(res) {
            $scope.response = res;
            var tmp1 = res.data;
            if($scope.response.data.error == true){
              $scope.showAlert(res.data.Message); 
            }else{
                if ($scope.jobs.length != tmp1.length) {
                MyJobs.setJobs(tmp1);
                console.log(tmp1);
                $scope.jobs = MyJobs.all();
                }
            }
        }).finally(function(){
        $ionicLoading.hide();
    });

                console.log($scope.jobs);
    };

    $scope.getMyJobs();
    $scope.viewSupplier = function(id){
        var suppData = Maps.getSupp(id);

        console.log("CLICKED: "+id);
        console.log(Maps.getSupp(id));

        console.log("VIEW SUPPLIER");
        console.log(suppData);
        $scope.supplierDetails.id = suppData.id;
        $scope.supplierDetails.suppEmail = suppData.email;
        $scope.supplierDetails.suppFname = suppData.first_name;
        $scope.supplierDetails.suppLname = suppData.last_name;
        $scope.supplierDetails.suppLat = suppData.lat;
        $scope.supplierDetails.suppLong = suppData.lng;

        console.log($scope.supplierDetails);


        $location.path("/app/supplier-profile");
    }


    $scope.showSuppliers = function(numSupp) {
        // if (window.cordova) {
        // cordova.plugins.diagnostic.isLocationEnabled(function(enabled) {
        //     alert("Location is " + (enabled ? "enabled" : "disabled"));
        // }, function(error) {
        //     alert("The following error occurred: " + error);
        // });
        // }
        var indexString = document.getElementById('jobType').value;
        var categorySelected = PostJob.getJobTypeId(indexString);
        console.log(categorySelected);

        navigator.geolocation.getCurrentPosition(function(pos) {
                $scope.tmpLat = pos.coords.latitude;
                $scope.tmpLng = pos.coords.longitude;
            }, function(error) {
                alert('Unable to get location: ' + error.message);
            });
        console.log("SHOW SUPPLIER");
        console.log($scope.tmpLat+", "+$scope.tmpLng);


        //$scope.suppliers = Maps.allSuppliers();
        $scope.show = "none";
        $scope.showList = "inline";

        //var categorySelected = document.getElementById('supplierJobs').selectedIndex+1;

         $ionicLoading.show({
          template: 'Searching...'
        })
        var linkGetNearestSupp = "lat="+$scope.tmpLat+"&lng="+$scope.tmpLng+"&lim="+numSupp+"&cat="+categorySelected+"";

        console.log($scope.tmpLat);
        console.log($scope.tmpLng);

        $http.post($scope.domainLink+"/user/findnearestsupplier", linkGetNearestSupp).then(function(res) {
        console.log(res.data);
        if(res.data.length==0){
            $ionicPopup.alert({
            title: 'No online suppliers near you.',
            template: 'Try changing the category. '
        });
        } else {
            console.log("SHOW SUPPPLIERS LIST");
            console.log(res.data);
            //loop for instant message to suppliers that was displayed/found
            var tmpSuppliers = res.data;
            var messageData = "Your profile was searched by a potential customer. ";

            for(var gd=0; gd<tmpSuppliers.length; gd++){
                var tmpSuppID = tmpSuppliers[gd].id; 
                var instantMsgToSupp= "memberid="+$scope.loginData.userid+"&supplierid="+tmpSuppID+"&sender="+$scope.loginData.userid+"&content="+messageData+"&jobid=0"+"";
            $http.post($scope.domainLink+"/messages/sendmessage", instantMsgToSupp).then(function(res) {
                    if(res.data.Status=="Success"){
                        console.log("SUCCESS INSTANT MSG TO SUPPLIER: ");
                    } else {
                        console.log("ERROR INSTANT MSG TO SUPPLIER: ");
                    }
                });
            }
            //end of instant message loop

            Maps.setSuppliers(res.data);
            //console.log(obj);
            $location.path("/app/maps");

            }
        })
        .finally(function(){
                var tmp= Maps.allSuppliers();
                $scope.suppliers= tmp;
                console.log("Waaaaaaaaaaaaaaaaaaa");
                console.log($scope.suppliers);

            $ionicLoading.hide();
        });
    }

    $scope.showMapView = function() {

    if($("#maplistBtn").html() == "Show List View"){
        $('#suppListbox').css('display',"inline");
        $('#suppMapbox').css('display',"none");
        $("#maplistBtn").html("Show Map View");
      } else if($("#maplistBtn").html() == "Show Map View"){
        $('#suppListbox').css('display',"none");
        $('#suppMapbox').css('display',"inline");
        $("#maplistBtn").html("Show List View");
      }


        var markers = [];
        var infoWindow = new google.maps.InfoWindow();

        // Event that closes the InfoWindow with a click on the map
        google.maps.event.addListener(map, 'click', function() {
            infoWindow.close();
        });

        initialize();

        function initialize() {
            var myLatlng = new google.maps.LatLng($scope.tmpLat, $scope.tmpLng);

            var mapOptions = {
                center: myLatlng,
                zoom: 14,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var map = new google.maps.Map(document.getElementById("map"),
                mapOptions);


            $scope.suppliersLoc = Maps.allSuppliers();
            //console.log(suppliersLoc.length);
            // Create some markers


            for (var i = 0; i < $scope.suppliersLoc.length; i++) {
                var location = new google.maps.LatLng($scope.suppliersLoc[i].lat, $scope.suppliersLoc[i].lng);
                createMarker($scope.suppliersLoc[i].email, location,$scope.suppliersLoc[i].id);
            }
            createMarker("You are here", myLatlng,"");

            function createMarker(name, latlng,idSupp) {
                console.log(idSupp);
                var marker;
                if (name == "You are here") {
                    marker = new google.maps.Marker({
                        map: map,
                        position: latlng,
                        icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png',
                        title: name
                    });
                } else {
                    marker = new google.maps.Marker({
                        map: map,
                        position: latlng,
                        title: name
                    });
                }


                // This event expects a click on a marker
                // When this event is fired the infowindow content is created
                // and the infowindow is opened
                google.maps.event.addListener(marker, 'click', function() {
                    if (name == "You are here") {
                        infoWindow.setContent("<div>You are here</div>");
                    } else {
                    // Variable to define the HTML content to be inserted in the infowindow
                    var clickString = 'ng-click=viewSupplier('+'"'+idSupp+'"'+')';
                    
                    var iwContent = '<div id="iw_container" '+clickString+'><strong>Supplier</strong><p>' + name + '</p></div>';
                    var compiled = $compile(iwContent)($scope);
                    console.log(compiled[0]);
                    // including content to the infowindow
                    infoWindow.setContent(compiled[0]);
                    }
                    // opening the infowindow in the current map and at the current marker location
                    infoWindow.open(map, marker);
                });
            }

            $scope.map = map;
        }
        google.maps.event.addDomListener(window, 'load', initialize);

        $scope.centerOnMe = function() {
            if (!$scope.map) {
                return;
            }

            $scope.loading = $ionicLoading.show({
                content: 'Getting current location...',
                showBackdrop: false
            });

            navigator.geolocation.getCurrentPosition(function(pos) {
                $scope.lat = pos.coords.latitude;
                $scope.longg = pos.coords.longitude;
                $scope.map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
                $ionicLoading.hide();
            }, function(error) {
                alert('Unable to get location: ' + error.message);
            });
        }

    }
    

    $scope.messageSupplier = function(){
        console.log("MESSAGE SUPPLIER with ID: "+$scope.supplierDetails.id);
        //console.log($scope.supplierDetails.id);
        var indexJobSelected = document.getElementById('jobsForSupp').value;
        var jobID = indexJobSelected;

        var replyPop = $ionicPopup.confirm({
            title: 'Message to Supplier',
            template: '<div><br><textarea id="replyData" maxlength="1000" rows="4" type="text" placeholder=" Message"></textarea></div>',
            cancelText: 'Cancel',
            okText: 'Send'
        });
        replyPop.then(function(res) {
            if(res) {
                //console.log('Send clicked');
                $ionicLoading.show({
                  template: 'Sending Message...'
                })

                console.log($("#replyData").val());
                var messageData = $("#replyData").val();

                var linkReply= "memberid="+$scope.loginData.userid+"&supplierid="+$scope.supplierDetails.id+"&sender="+$scope.loginData.userid+"&content="+messageData+"&jobid="+jobID+"";
            $http.post($scope.domainLink+"/messages/sendmessage", linkReply).then(function(res) {
                    if(res.data.Status=="Success"){
                        $ionicLoading.hide();
                        $ionicLoading.show({
                          template: 'Message Sent'
                        })
                        $timeout(function() {
                            $ionicLoading.hide();
                        }, 1000);
                    } else {
                        $ionicLoading.hide();
                        $ionicLoading.show({
                          template: 'Error sending message.'
                        })
                        $timeout(function() {
                            $ionicLoading.hide();
                        }, 1000);
                    }
                })
                .finally(function(){
                    $ionicLoading.hide();
                });
            } else {
                //console.log('dnt delete');

            }
        })
    }
});