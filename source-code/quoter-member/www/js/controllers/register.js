angular.module('starter.controllers')
.controller('RegisterCtrl', function(PostJob, $scope,$ionicLoading,$ionicPlatform,$cordovaOauth,$ionicModal,$interval, $ionicPopover, $http, $timeout,  $location, $ionicPopup) {

	    $scope.registerUser = function(details) {
        var error = "";
        if(details!=undefined){
            if(details.username == undefined){
            error = error+" missing username.";
            }
            if(details.password == undefined){
                error = error+" missing password.";
            }
            if(details.fName == undefined){
                error = error+" missing first name.";
            }
            if(details.lName == undefined){
                error = error+" missing last name.";
            }
            if(details.eAdd == undefined){
                error = error+" missing email address.";
            }
        } else {
            error = error+"missing details."
        }
        
        console.log(error);

        if (error.length==0) {
            var linkReg="username="+details.username+"&password="+details.password+"&first_name="+details.fName+"&last_name="+details.lName+"&email="+details.eAdd+"";
            $ionicLoading.show({
                  template: 'Registering...'
                })
            $http.post($scope.domainLink+"/user/register", linkReg).then(function (res){
                console.log(res.data);
                $scope.response = res;

                if(res.data.Status=="Success"){
                        $ionicPopup.alert({
                            title: 'Success',
                            template: "You are now registered!"
                        }); 
                        $location.path('/app/login');
                    } else {
                        $ionicPopup.alert({
                            title: 'Error',
                            template: res.data.Error
                        }); 
                    }
            })
            .finally(function(){
                    $ionicLoading.hide();
                });
            } else {
                $ionicPopup.alert({
                    title: 'Error',
                    template: "All fields are required."
                }); 
            }
        }

});