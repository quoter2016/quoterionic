angular.module('starter.services', [])

.factory('MyJobs', function() {

  var jobs = [];

  return {
    all: function() {
      return jobs;
    },
    get: function(jobId) {
      for (var i = 0; i < jobs.length; i++) {
        console.log(jobs[i].jobid);
        if (jobs[i].jobid === jobId) {
          return jobs[i];
        }
      }
      return null;
    },
    setJobs : function(jobRecords){
      jobs = jobRecords;

    }
  };
})

.factory('PostJob', function() {

  var categories = [];
  var jobtypes = [];

  return {
    setJobTypes: function(obj) {
      jobtypes = obj;
      //console.log(obj.Jobtypes[0]);
    },
    allJobtypes: function() {
      return jobtypes;
    }, 
    all: function() {
      return categories;
    },
    getJobTypeId: function(str) {
      for (var i = 0; i < jobtypes.length; i++) {
        if (jobtypes[i].description == str) {
          return jobtypes[i].id;
        }
      }
      return null;
    },
    setCategories : function(obj){
      categories = obj;
    }
  };
})
.factory('Maps', function() {

  var supplierJobs = [];
  var suppliers = [];
  return {
    all: function() {
      return supplierJobs;
    },
    get: function(id) {
      for (var i = 0; i < supplierJobs.length; i++) {
        if (supplierJobs[i].id === parseInt(id)) {
          return supplierJobs[i];
        }
      }
      return null;
    },
    getSupp: function(id) {
      for (var i = 0; i < suppliers.length; i++) {
        if (suppliers[i].id == id) {
          return suppliers[i];
        }
      }
      return null;
    },
    setCategories : function(obj){
      supplierJobs = obj;
    },
    allSuppliers: function() {
      return suppliers;
    },
    setSuppliers : function(obj){
      suppliers = [];
      var totalSuppliers = obj.length;
      for (var i = 0; i < totalSuppliers; i++) {
        suppliers.push(obj[i]);
        //jobs=jobs.concat(obj[i].Record);
      }
      //categories = categories.splice(0, 1);
    }
  };
})
.factory('Messages', function() {
  var receivedMsgs = [];
  var sentMsgs = [];

  return {
    all: function() {
      return receivedMsgs;
    },
    get: function(id) {
      for (var i = 0; i < receivedMsgs.length; i++) {
        if (receivedMsgs[i].id === id) {
          return receivedMsgs[i];
        }
      }
      return null;
    },
    setMsgs : function(msg){
      receivedMsgs = msg;
    },
    allSent: function() {
      return sentMsgs;
    },
    setMsgsSent : function(msg){
      sentMsgs = msg;
    },
    getSent: function(id) {
      for (var i = 0; i < sentMsgs.length; i++) {
        if (sentMsgs[i].id === id) {
          return sentMsgs[i];
        }
      }
      return null;
    }
  };
})

.factory('FilteredJobs', function() {

  var jobs = [];

  return {
    all: function() {
      return jobs;
    },
    get: function(jobId) {
      for (var i = 0; i < jobs.length; i++) {
        console.log(jobs[i].jobid);
        if (jobs[i].jobid === jobId) {
          return jobs[i];
        }
      }
      return null;
    },
    setJobs : function(jobRecords){
      jobs = jobRecords;

    },
    pushJob : function(record){
      console.log("PUSH");
        jobs.push(record);
    },
    erase : function(record){
        jobs = [];
    }
  };
})

.factory('LocationData', function() {

  var locationInput = [];

  return {
    all: function() {
      return locationInput;
    },
    setLoc : function(data){
      locationInput = [];

      locationInput.lat = data.lat;
      locationInput.lng = data.lng;

      console.log(locationInput);

    }
  };
});
