// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers' , 'starter.services'])

.run(function($ionicPlatform , $rootScope, $timeout) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });

     $rootScope.authStatus = false;
	 //stateChange event
	  $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){
		  $rootScope.authStatus = toState.authStatus;
		  if($rootScope.authStatus){
			  
			
		  }
    });

	$rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
		console.log("URL : "+toState.url);
		if(toState.url=='/member-menu'){
			console.log("match : "+toState.url);
			$timeout(function(){
				angular.element(document.querySelector('#leftMenu' )).removeClass("hide");
			},1000);
		}	
	});

})

.config(function($stateProvider,$httpProvider, $urlRouterProvider) {
  
$httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
  $stateProvider
    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

//--------------------------------------

 .state('app.login', {
    url: '/login',
    views: {
      'menuContent': {
        templateUrl: 'templates/tab-signin.html',
        controller: 'LoginCtrl'
      }
    },
	authStatus: false
  })
 .state('app.signup', {
    url: '/signup',
    views: {
      'menuContent': {
        templateUrl: 'templates/tab-signup.html',
        controller: 'RegisterCtrl'
      }
   },
	authStatus: false
  })
//--------------------------------------


  .state('app.memberMenu', {
    url: '/member-menu',
    views: {
      'menuContent': {
        templateUrl: 'templates/member-menu.html',
		    //controller: 'MemberMenuCtrl'
      }
     },
	 authStatus: true
  })

  .state('app.postJob', {
    url: '/post-job',
    views: {
      'menuContent': {
        templateUrl: 'templates/post-job.html',
        controller: 'PostJobCtrl'
      }
     }
  })

  .state('app.myJobs', {
    url: '/my-jobs',
    views: {
      'menuContent': {
        templateUrl: 'templates/my-jobs.html',
        controller: 'MyJobsCtrl'
      }
     }
  })

  .state('app.messages', {
    url: '/messages',
    views: {
      'menuContent': {
        templateUrl: 'templates/messages.html',
        controller: 'MessagesCtrl'
      }
     }
  })

  .state('app.fullmsg', {
    url: '/full-msg',
    views: {
      'menuContent': {
        templateUrl: 'templates/full-msg.html',
        controller: 'MessagesCtrl'
      }
     }
  })
  .state('app.fullmsgSent', {
    url: '/full-msgSent',
    views: {
      'menuContent': {
        templateUrl: 'templates/full-msgSent.html',
        controller: 'MessagesCtrl'
      }
     }
  })

  .state('app.findSupplier', {
    url: '/find-supplier',
    views: {
      'menuContent': {
        templateUrl: 'templates/find-supplier.html',
        controller: 'MapsCtrl'
      }
     }
  })
  .state('app.maps', {
    url: '/maps',
    views: {
      'menuContent': {
        templateUrl: 'templates/maps.html',
        controller: 'MapsCtrl'
      }
     }
  })
  .state('app.supplierProf', {
    url: '/supplier-profile',
    views: {
      'menuContent': {
        templateUrl: 'templates/supplier-profile.html',
        controller: 'MapsCtrl'
      }
     }
  })
  .state('app.preferences', {
    url: '/preferences',
    views: {
      'menuContent': {
        templateUrl: 'templates/preferences.html',
        controller: 'PreferencesCtrl'
      }
     }
  })

  .state('app.setLoc', {
    url: '/set-location',
    views: {
      'menuContent': {
        templateUrl: 'templates/set-location.html',
        controller: 'SetLocationCtrl'
      }
     }
  })
  .state('app.job-information', {
    url: '/job-information',
    views: {
      'menuContent': {
        templateUrl: 'templates/job-information.html',
        controller: 'JobInformationCtrl'
      }
     }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/login');
});
