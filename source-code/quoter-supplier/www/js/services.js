angular.module('starter.services', [])

.factory('MyJobs', function() {

  var jobs = [];

  return {
    all: function() {
      return jobs;
    },
    get: function(jobId) {
      for (var i = 0; i < jobs.length; i++) {
        console.log(jobs[i].id);
        if (jobs[i].id === jobId) {
          return jobs[i];
        }
      }
      return null;
    },
    setJobs : function(jobRecords){
      jobs = jobRecords;

    }
  };
})

.factory('FindJobs', function() {
  var jobsFound = [];

  return {
    all: function() {
      return jobsFound;
    },
    get: function(jobId) {
      for (var i = 0; i < jobsFound.length; i++) {
        console.log(jobsFound[i].jobid);
        if (jobsFound[i].jobid === jobId) {
          return jobsFound[i];
        }
      }
      return null;
    },
    setJobs : function(jobRecords){
      jobsFound = jobRecords;

    }
  };
})

.factory('PostJob', function() {

  var categories = [];
  var jobtypes = [];

  return {
    setJobTypes: function(obj) {
      jobtypes = obj;
      //console.log(obj.Jobtypes[0]);
    },
    allJobtypes: function() {
      return jobtypes;
    }, 
    all: function() {
      return categories;
    },
    getJobTypeId: function(str) {
      for (var i = 0; i < jobtypes.length; i++) {
        if (jobtypes[i].description == str) {
          return jobtypes[i].id;
        }
      }
      return null;
    },
    setCategories : function(obj){
      categories = obj;
    }
  };
})

.factory('Messages', function() {
  var receivedMsgs = [];
  var sentMsgs = [];
  return {
    all: function() {
      return receivedMsgs;
    },
    get: function(id) {
      for (var i = 0; i < receivedMsgs.length; i++) {
        if (receivedMsgs[i].id === id) {
          return receivedMsgs[i];
        }
      }
      return null;
    },
    setMsgs : function(msg){
      receivedMsgs = msg;
    },
    allSent: function() {
      return sentMsgs;
    },
    setMsgsSent : function(msg){
      sentMsgs = msg;
    },
    getSent: function(id) {
      for (var i = 0; i < sentMsgs.length; i++) {
        if (sentMsgs[i].id === id) {
          return sentMsgs[i];
        }
      }
      return null;
    }
  };
})

.factory('FilteredJobs', function() {

  var jobs = [];

  return {
    all: function() {
      return jobs;
    },
    get: function(jobId) {
      for (var i = 0; i < jobs.length; i++) {
        console.log(jobs[i].jobid);
        if (jobs[i].jobid === jobId) {
          return jobs[i];
        }
      }
      return null;
    },
    setJobs : function(jobRecords){
      jobs = jobRecords;

    },
    pushJob : function(record){
      console.log("PUSH");
        jobs.push(record);
    },
    erase : function(record){
        jobs = [];
    }
  };
});
