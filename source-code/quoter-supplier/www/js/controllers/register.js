angular.module('starter.controllers')
.controller('RegisterCtrl', function(PostJob, $scope,$ionicLoading,$ionicPlatform,$cordovaOauth,$ionicModal,$interval, $ionicPopover, $http, $timeout,  $location, $ionicPopup) {
    $(document).on('change', '#jobCategory', function() {
        var index = document.getElementById('jobCategory').selectedIndex+1;
        console.log(index);
        $ionicLoading.show({
          template: 'Loading Job Types...'
        })
        $scope.loadCategories();
    });

    $ionicLoading.show({
      template: 'Loading Job Categories...'
    })
    var linkGetCat = "parent=0";
    $http.post($scope.domainLink+"/admin/getalljobtypes", linkGetCat).then(function(res) {
        PostJob.setCategories(res.data);
        // $timeout(function() {
        $scope.categories = PostJob.all();
        console.log($scope.categories);
        // }, 2000);
    //$scope.loadCategories();
    })
    .finally(function(){
        $ionicLoading.hide();
    });


    //initial types
    $ionicLoading.show({
          template: 'Loading Job Types...'
        })
        var linkGetJobTypes = "parent=1";
        $http.post($scope.domainLink+"/admin/getalljobtypes", linkGetJobTypes).then(function(res) {
            PostJob.setJobTypes(res.data);

            //$timeout(function() {
                $scope.jobtypes = PostJob.allJobtypes();
            //}, 500);
        })
        .finally(function(){
            $ionicLoading.hide();
        });
    //end initial jobtypes

    $scope.loadCategories = function() {
        $ionicLoading.show({
          template: 'Loading Job Types...'
        })
        var index = document.getElementById('jobCategory').selectedIndex+1;
        var linkGetJobTypes = "parent="+index;
        $http.post($scope.domainLink+"/admin/getalljobtypes", linkGetJobTypes).then(function(res) {
            PostJob.setJobTypes(res.data);

            //$timeout(function() {
                $scope.jobtypes = PostJob.allJobtypes();
            //}, 500);
        })
        .finally(function(){
            $ionicLoading.hide();
        });
    };

    $scope.registerUser = function(details) {
        var error = "";
        var indexString = document.getElementById('jobType').value;
        
        if(details!=undefined){
            details.categoryId = PostJob.getJobTypeId(indexString);
            console.log(details.categoryId);
            details.lat = $("#latIn").val();
            details.lng = $("#longIn").val();
            if(details.username == undefined){
            error = error+" missing username.";
            }
            if(details.password == undefined){
                error = error+" missing password.";
            }
            if(details.fName == undefined){
                error = error+" missing first name.";
            }
            if(details.lName == undefined){
                error = error+" missing last name.";
            }
            if(details.eAdd == undefined){
                error = error+" missing email address.";
            }
            if(details.lat == ''){
                error = error+" missing latitude.";
            } 
            if(details.lng == ''){
                error = error+" missing longitude.";
            }
        } else {
            error = error+"missing details."
        }
        
        console.log(error);

        if (error.length==0) {
          console.log("all fields completed");
            var linkReg="username="+details.username+"&password="+details.password+"&first_name="+details.fName+"&last_name="+details.lName+"&email="+details.eAdd+""+"&lat="+details.lat+""+"&lng="+details.lng+""+"&categoryid="+details.categoryId+"";
            $ionicLoading.show({
                  template: 'Registering...'
                })
            $http.post($scope.domainLink+"/supplier/register", linkReg).then(function (res){
                console.log(res.data);
                $scope.response = res;

                    if(res.data.Status=="Success"){
                        $ionicPopup.alert({
                            title: 'Success',
                            template: "You are now registered!"
                        }); 
                        $location.path('/app/login');
                    } else {
                        $ionicPopup.alert({
                            title: 'Error',
                            template: res.data.Error
                        }); 
                    }
            })
            .finally(function(){
                    $ionicLoading.hide();
                });
            } else {
                $ionicPopup.alert({
                    title: 'Error',
                    template: "All fields are required."
                }); 
            }
    }
});