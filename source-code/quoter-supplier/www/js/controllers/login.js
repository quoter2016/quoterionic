angular.module('starter.controllers')
.controller('LoginCtrl', function(PostJob, $scope,$ionicLoading,$ionicPlatform,$cordovaOauth,$ionicModal,$interval, $ionicPopover, $http, $timeout,  $location, $ionicPopup) {

	 $scope.login = function(user) {
		if(typeof(user)=='undefined'){
			$scope.showAlert('Please fill username and password to proceed.');	
			return false;
		} else {
            $(document).ready(function() {
                if ($('#stayIn').is(':checked')) {
                    user.rememberme = 1;
                } else {
                    user.rememberme = 0;
                }
            }); 


            $ionicLoading.show({
              template: 'Logging in...'
            })
            var linkLogin = "username="+user.username+"&password="+user.password+"&rememberme="+user.rememberme+"";
            $http.post($scope.domainLink+"/supplier/login", linkLogin).then(function (res){
            console.log(res.data);
            $scope.response = res;

            if($scope.response.data.error!=undefined){
              $scope.showAlert(res.data.status); 
            }else{
                $scope.loginData.acessTokenId = res.data.id;
                $scope.loginData.accessToken = user.access_token;
                $scope.loginData.username = user.username;
                $scope.loginData.password = user.password;
                $scope.loginData.userid = res.data.user.id;
                $scope.loginData.lat = res.data.user.location.lat;
                $scope.loginData.lng = res.data.user.location.lng;
                $scope.loginData.cat = res.data.categoryid;
                //console.log("DATAAAAA "+$scope.loginData.userid);
                //console.log(res.data);
                $location.path('/app/supplier-menu');
            }
        })
        .finally(function(){
                $ionicLoading.hide();
            });
        }
		
	}

	$scope.googleLogin = function() {
        navigator.geolocation.getCurrentPosition(function(pos) {
            $scope.supplierLat = pos.coords.latitude;
            $scope.supplierLng = pos.coords.longitude;
            console.log("LAT: "+$scope.supplierLat+" LNG: "+$scope.supplierLng);
            $cordovaOauth.google("536258911748-935ajmj6udftjgahp1namej20nvfpfd5.apps.googleusercontent.com", ["email"]).then(function(result) {
                console.log(JSON.stringify(result));
                var tokenGoogle = result.access_token;
                $scope.registerGoogle(tokenGoogle);

            }, function(error) {
                $ionicPopup.alert({
                     title: 'Error',
                     template: JSON.stringify(error)
                   });
                console.log(error);
            });

        }, function(error) {
            $ionicPopup.alert({
                     title: 'Error',
                     template: 'Unable to get location: ' + error.message
            });
        });

    }
    $scope.facebookLogin = function (){
        navigator.geolocation.getCurrentPosition(function(pos) {
            $scope.supplierLat = pos.coords.latitude;
            $scope.supplierLng = pos.coords.longitude;
            console.log("LAT: "+$scope.supplierLat+" LNG: "+$scope.supplierLng);
            $cordovaOauth.facebook("154214858324901", ["email"]).then(function(result){
                  $scope.registerFacebook(result.access_token);
                console.log(JSON.stringify(result));
            }, function(error) {
                $ionicPopup.alert({
                     title: 'Facebook Error',
                     template: JSON.stringify(error)
                   });
                //console.log(JSON.stringify(error));
            });

        }, function(error) {
            $ionicPopup.alert({
                     title: 'Error',
                     template: 'Unable to get location: ' + error.message
                   });
        });

    }

    $scope.registerFacebook = function(access_token) {
      var tok = access_token;
      $ionicLoading.show({
              template: 'Please wait...'
            })
    $http.get("https://graph.facebook.com/v2.2/me", {params: {access_token: access_token, fields: "id,email,first_name, last_name", format: "json" }}).then(function(result) {
        var profile = result.data;
        // $ionicPopup.alert({
        //          title: '',
        //          template: "DATA RETURN FROM FB"+profile.id+"<br>"+profile.email+"<br>"+profile.first_name+"<br>"+profile.last_name
        //        });
    $scope.facebookToServer(profile,tok);
    }, function(error) {
        $ionicLoading.hide();
          $ionicPopup.alert({
                   title: 'Error',
                   template: JSON.stringify(error)
                 });
        alert("Error: " + error);
    });

	}
  $scope.facebookToServer = function(dataFromFB,at){
    var data = dataFromFB;
    var access_token = at;
    $ionicLoading.hide();
    $ionicLoading.show({
              template: 'Logging in...'
            })
            var linkLogin = "first_name="+data.first_name+"&last_name="+data.last_name+"&email="+data.email+"&access_token="+access_token+"&categoryid="+"1"+"&lat="+$scope.supplierLat+"&lng="+$scope.supplierLng+"&login_type="+"facebook"+"";
            $http.post($scope.domainLink+"/social/supplierlogin", linkLogin).then(function (res){
                //$ionicLoading.hide();
                $scope.loginData.acessTokenId = res.data.id;
                $scope.loginData.accessToken = res.data.access_token;
                $scope.loginData.username = res.data.user.email;
                $scope.loginData.userid = res.data.user.id;
                $scope.loginData.lat = res.data.user.location.lat;
                $scope.loginData.lng = res.data.user.location.lng;
                $scope.loginData.cat = res.data.categoryid;

                $location.path('/app/supplier-menu');

        })
        .finally(function(){
                $ionicLoading.hide();
            });
  }

	//========
	$scope.registerGoogle = function(access_token) {
        $ionicLoading.show({
              template: 'Please wait...'
            })
	    $http({method:"GET", url:"https://www.googleapis.com/plus/v1/people/me?access_token="+access_token}).
	          success(function(response){
	            // $ionicPopup.alert({
	            //      title: '',
	            //      template: JSON.stringify(response)
	            //    });
              $scope.googleToServer(response,access_token);
	          }, function(error) {
                $ionicLoading.hide();
	            $ionicPopup.alert({
	                 title: '',
	                 template: error
	               });
	          console.log(error);
	        });

	}

  $scope.googleToServer = function(data,access_token){
    $ionicLoading.hide();
    $ionicLoading.show({
              template: 'Logging in...'
            })
            var linkLogin = "first_name="+data.name.givenName+"&last_name="+data.name.familyName+"&email="+data.emails[0].value+"&access_token="+access_token+"&categoryid="+"1"+"&lat="+$scope.supplierLat+"&lng="+$scope.supplierLng+"&login_type="+"google"+"";
            $http.post($scope.domainLink+"/social/supplierlogin", linkLogin).then(function (res){
                //$ionicLoading.hide();
                $scope.loginData.acessTokenId = res.data.id;
                $scope.loginData.accessToken = res.data.access_token;
                $scope.loginData.username = res.data.user.email;
                $scope.loginData.userid = res.data.user.id;
                $scope.loginData.lat = res.data.user.location.lat;
                $scope.loginData.lng = res.data.user.location.lng;
                $scope.loginData.cat = res.data.categoryid;
                
                // $ionicPopup.alert({
                //    title: '',
                //    template: JSON.stringify(res)
                //  });

        })
        .finally(function(){
                $ionicLoading.hide();
                $location.path('/app/supplier-menu');
            });
  }

  });