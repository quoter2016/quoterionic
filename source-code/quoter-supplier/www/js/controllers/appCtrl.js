angular.module('starter.controllers')
.controller('AppCtrl', function(PostJob, $scope,$ionicLoading,$ionicPlatform,$cordovaOauth,$ionicModal,$interval, $ionicPopover, $http, $timeout,  $location, $ionicPopup) {

      $scope.domainLink = "http://149.56.98.5/api";
      $scope.loginData = {};
      $scope.supplierLat = "";
      $scope.supplierLng = "";
      $scope.jobDataClicked = {};
      $http.defaults.headers.post["Content-Type"] = 'application/x-www-form-urlencoded;charset=utf-8;';

    // $scope.checkAccessToken = function () {
    //     console.log("CHECKING ACCESS TOKEN IF ONLINE");
    //     var notexpired = false;
    //     var accessID= "id="+$scope.loginData.acessTokenId+"";
    //      $http.post($scope.domainLink+"/preferences/isexpire", accessID).then(function(res) {
    //                 if(res.data.expire==true){
    //                     console.log("TOKEN EXPIRED");
    //                     $scope.loginAgain();
    //                 }
    //             });
    // }

    $scope.loginAgain = function () {
        if (angular.isDefined($scope.Timer)) {
            $interval.cancel($scope.Timer);
        }
        $ionicPopup.alert({
         title: 'Please login first',
         template: ''
       });
        $location.path('/app/login');
    }


    $scope.setLocSupplier = function(){
        navigator.geolocation.getCurrentPosition(function(pos) {
            $scope.supplierLat = pos.coords.latitude;
            $scope.supplierLng = pos.coords.longitude;
                    $("#latIn").val($scope.supplierLat);
                    $("#longIn").val($scope.supplierLng);
        }, function(error) {
            alert('Unable to get location: ' + error.message);
        });
    }


  //--------------------------------------------
  $scope.logout = function() {   
    var suppId= "id="+$scope.loginData.userid+"";
    // $http.post($scope.domainLink+"/preferences/logout", suppId).then(function(res) {
    //                 if(res.data.status==true){
    //                     $ionicPopup.alert({
    //                      title: 'You are now logged out!',
    //                      template: ''
    //                    });
    //                     $location.path('/app/login');   
    //                 } else {

    //                 }
    //             });
      $ionicPopup.alert({
       title: 'You are now logged out!',
       template: ''
     });
      $location.path('/app/login');   
    }

    $scope.showAlert = function(msg) {
       var alertPopup = $ionicPopup.alert({
         title: 'Warning Message',
         template: msg
       });
    }

  });