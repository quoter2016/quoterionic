angular.module('starter.controllers')
.controller('FindJobsCtrl', function($scope,$location,$timeout, $stateParams,$ionicLoading, $interval, $ionicPopup, $http, FindJobs) {
    $scope.jobsFound=FindJobs.all();
    $ionicLoading.show({
          template: 'Searching Jobs...'
        })
    console.log("FIND JOBS");
    console.log("YOUR LOCATION "+$scope.loginData.lat+", "+$scope.loginData.lng);

        var linkGetCat = "lat="+$scope.loginData.lat+"&lng="+$scope.loginData.lng+"&lim=1000"+"&cat="+$scope.loginData.cat+"";
        $http.post($scope.domainLink+"/supplier/findnearestjobs", linkGetCat).then(function(res) {

            FindJobs.setJobs(res.data);
            $scope.jobsFound=FindJobs.all();
            console.log("JOBS FOUUUUUUUUUUND");
            console.log($scope.jobsFound);
            if($scope.jobsFound.length==0){
                console.log("NO JOOOOOOOBS FOUND");
                $ionicPopup.alert({
                    title: 'Job Search',
                    template: "No jobs near you."
                });
                $location.path('/app/supplier-menu');
            }

        })
        .finally(function(){
            $ionicLoading.hide();
    });

    $scope.showJobInfo = function(jobData) {
        $scope.jobDataClicked.data = jobData; //to be used on JobInformationCtrl
        $location.path("/app/job-information");
    }

});