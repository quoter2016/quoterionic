angular.module('starter.controllers')
.controller('PreferencesCtrl', function($scope, $stateParams,$ionicLoading, $interval, $ionicPopup, $http) {
    $scope.toggleOnline = "";
    $ionicLoading.show({
          template: 'Loading Preferences...'
        })
    var getStatus = "id="+$scope.loginData.userid+"";
            $http.post($scope.domainLink+"/preferences/getonline", getStatus).then(function(res) {
                console.log("STATUS");
                console.log(res.data.toggle);
                $scope.toggleOnline = res.data.toggle;
                if($scope.toggleOnline == 1){
                    $("#statusToggle").prop( "checked", true);
                } else {
                    $("#statusToggle").prop( "checked", false);
                }
            })
            .finally(function(){
                $ionicLoading.hide();
        });

    $('#statusToggle').change(function() {
       if($(this).is(":checked")) {
          console.log("checked");
          $scope.setStatus("1","You are now online");
       } else {
            console.log("unchecked");
            $scope.setStatus("0","You are now offline");
       }
    });

    $scope.setStatus = function(toggle,msg){
        var setStat = "id="+$scope.loginData.userid+"&toggle="+toggle+"";
            $http.post($scope.domainLink+"/preferences/setonline", setStat).then(function(res) {
                if(res.data.status == "success"){
                    $ionicPopup.alert({
                        title: 'Status Changed',
                        template: msg
                    });
                }
            })
            .finally(function(){
                $ionicLoading.hide();
        });

    }

    $scope.changePassword = function(data){
            if(typeof(data)!='undefined'){
                if(data.passNew==data.passNewRep){
                    $ionicLoading.show({
                      template: 'Changing Password...'
                    })
                    var linkLogin = "id="+$scope.loginData.userid+"&password="+data.passNew+"";
                    $http.post($scope.domainLink+"/preferences/changepassword", linkLogin).then(function (res){
                        console.log(res.data);
                        $scope.response = res;

                        if($scope.response.data.error!=undefined){
                          $scope.showAlert(res.data.status); 
                        }else{
                            //$location.path('/app/member-menu');
                        }
                        if(res.data.status=="SUCCESS"){
                            $ionicPopup.alert({
                                title: 'Success',
                                template: "Password changed successfully!"
                            }); 
                        }
                    })
                    .finally(function(){
                            $ionicLoading.hide();
                    });
                } else {
                $ionicPopup.alert({
                    title: 'ERROR',
                    template: "Password does not match."
                }); 
            }
        } else {
            $ionicPopup.alert({
                title: 'ERROR',
                template: "All fields are required."
            }); 
        }
    }
});