angular.module('starter.controllers')
.controller('MessagesCtrl', function($scope,$ionicScrollDelegate,$location,$ionicLoading,$interval, $stateParams, $http, $timeout, $ionicPopup, Messages,FilteredJobs) {
    $scope.receivedMsgs = Messages.all();
    $scope.filteredJobs = FilteredJobs.all();

    if($location.path()=="/app/full-msg"){
        var currentMsg = Messages.get(currMsgid);
        $("#viewSenderEmail").html(currentMsg.sender_email);
        $("#viewSenderName").html(currentMsg.sender_name);
        $("#viewSenderUsername").html(currentMsg.sender_username);
        $("#viewDate").html(currentMsg.timestamp);
        $("#viewJobID").html(currentMsg.jobid);
        //$("#viewSubject").html(currentMsg.Record.subject);
        $("#viewMessage").val(currentMsg.content);

    } else if ($location.path()=="/app/full-msgSent"){
        var currentSentMsgs = Messages.getSent(currMsgid);
        console.log(currentSentMsgs);
        // $("#viewReceiverEmail").html(currentrentMsg.sender_email);
        // $("#viewReceiverName").html(currentMsg.sender_name);
        // $("#viewReceiverUsername").html(currentMsg.sender_username);
        $("#viewSenderId").html(currentSentMsgs.supplierid);
        $("#viewDateReceiver").html(currentSentMsgs.timestamp);
        $("#viewJobIDReceiver").html(currentSentMsgs.jobid);
        // //$("#viewSubject").html(currentMsg.Record.subject);
        $("#viewMessageReceiver").val(currentSentMsgs.content);
    } else{
        $ionicLoading.show({
          template: 'Loading Messages...'
        })
    }

    $scope.viewAll = function() {
        $('#allMsgsByJobPanel').css('display',"none");
        $('#allMsgsSentPanel').css('display',"none");
        $('#allMsgsPanel').css('display',"inline");
        $('#btnViewAll').addClass('active');
        $('#btnViewJobs').removeClass('active');
        $('#btnViewAllSent').removeClass('active');
    }
    $scope.viewAllSent = function() {
        $('#allMsgsByJobPanel').css('display',"none");
        $('#allMsgsPanel').css('display',"none");
        $('#allMsgsSentPanel').css('display',"inline");

        $('#btnViewAll').removeClass('active');
        $('#btnViewJobs').removeClass('active');
        $('#btnViewAllSent').addClass('active');
    }

    $scope.viewByJobs = function() {
        $('#allMsgsByJobPanel').css('display',"inline");
        $('#allMsgsPanel').css('display',"none");
        $('#allMsgsSentPanel').css('display',"none");
        $('#btnViewAll').removeClass('active');
        $('#btnViewJobs').addClass('active');
        $('#btnViewAllSent').removeClass('active');

        var linkGetJobs = "id="+$scope.loginData.userid;
            $http.post($scope.domainLink+"/supplier/getjobsbysupplierid", linkGetJobs).then(function(res) {
            $scope.response = res;
            var tmp1 = res.data;

            FilteredJobs.setJobs(tmp1);
            $scope.filteredJobs = FilteredJobs.all();

            console.log($scope.filteredJobs);
            var tmpJobs = $scope.filteredJobs;
            console.log("TMPPPPP");
            console.log(tmpJobs);

            var deleteThis,currRecord,currMsg;
            FilteredJobs.erase();
            for (var i = 0;i<tmpJobs.length; i++) {
                 deleteThis = true;
                 currRecord = tmpJobs[i];
                    for (var j = 0;j<$scope.receivedMsgs.length; j++) {
                         currMsg = $scope.receivedMsgs[j];
                         console.log("TRY MATCH: "+currRecord.id+" = "+currMsg.jobid);
                        if (currRecord.id == currMsg.jobid) {
                            deleteThis = false;
                        }
                    }
                    if (deleteThis == false ) {
                        FilteredJobs.pushJob(tmpJobs[i]);
                        console.log("MAAAAAAAAAAAAAAATCH");
                        console.log(tmpJobs[i]);
                    }

            } $scope.filteredJobs = FilteredJobs.all();
            console.log("finaaaaaaaaaaaaaaaaaaal");
            console.log($scope.filteredJobs);
            });
    }
    $scope.showMoreMsg = function(data) {   
        var msgClicked = data.id;
        currMsgid = msgClicked;
        console.log("CLICKED: "+currMsgid);
        $location.path("/app/full-msg");
    }
    $scope.showMoreMsgSent = function(data) {
        var msgClicked = data.id;
        currMsgid = msgClicked;
        console.log("CLICKED: "+currMsgid);
        $location.path("/app/full-msgSent");
    }

    $scope.replyMsg = function(data){
        console.log("REPLYYYYYY");
        console.log(data);
        var linkReply= "";  

            var replyPop = $ionicPopup.confirm({
                title: 'Message Reply',
                template: '<div><br><textarea id="replyData" maxlength="1000" rows="4" type="text" placeholder=" Message"></textarea></div>',
                cancelText: 'Cancel',
                okText: 'Send'
            });
            replyPop.then(function(res) {
                if(res) {
                    //console.log('Delete clicked');
                    $ionicLoading.show({
                      template: 'Sending Message...'
                    })

                    console.log($("#replyData").val());
                    var messageData = $("#replyData").val();

                if(data == '1'){
                    console.log("REPLY FROM FULL MSG");
                    var msgRep = Messages.get(currMsgid);
                    linkReply = "memberid="+msgRep.memberid+"&supplierid="+msgRep.supplierid+"&sender="+msgRep.supplierid+"&content="+messageData+"&jobid="+msgRep.jobid+""; 
                }
                else {
                    linkReply= "memberid="+data.memberid+"&supplierid="+data.supplierid+"&sender="+data.supplierid+"&content="+messageData+"&jobid="+data.jobid+""; 
                }

                $http.post($scope.domainLink+"/messages/sendmessage", linkReply).then(function(res) {
                        if(res.data.Status=="Success"){
                            $ionicLoading.hide();
                            $ionicLoading.show({
                              template: 'Reply Sent'
                            })
                            $timeout(function() {
                                $ionicLoading.hide();
                            }, 1000);
                        } else {
                            $ionicLoading.hide();
                            $ionicLoading.show({
                              template: 'Error sending message.'
                            })
                            $timeout(function() {
                                $ionicLoading.hide();
                            }, 1000);
                        }
                    })
                    .finally(function(){
                    });
                } else {
                    //console.log('dnt delete');

                }
            });
    }

    $scope.deleteMsg = function(data){
        var linkDelete="";

            var deleteMsgSure = $ionicPopup.confirm({
                title: 'Delete Message',
                template: 'Are you sure to delete this message?',
                cancelText: 'Cancel',
                okText: 'Ok'
            });
            deleteMsgSure.then(function(res) {
                if(res) {
                    //console.log('Delete clicked');
                    $ionicLoading.show({
                      template: 'Deleting Message...'
                    })
                    console.log(data);  
                    var l

                    if(data == '1'){
                    console.log("delete FROM FULL MSG");
                    linkDelete= "id="+currMsgid+"";
                    }
                    else {
                        linkDelete= "id="+data.id+"";
                    }
                
                $http.post($scope.domainLink+"/messages/deletemessage", linkDelete).then(function(res) {
                        if(res.data.Status=="Success"){
                            $ionicLoading.hide();
                            $ionicLoading.show({
                              template: 'Message deleted'
                            })
                            $timeout(function() {
                                $ionicLoading.hide();
                            }, 1000);
                            $location.path('/app/messages');
                        } else {
                            $ionicLoading.hide();
                            $ionicLoading.show({
                              template: 'Error deleting message.'
                            })
                            $timeout(function() {
                                $ionicLoading.hide();
                            }, 1000);
                        }
                    })
                    .finally(function(){
                    });
                } else {
                    //console.log('dnt delete');

                }
            });
    }
    
    $scope.firstLoadMessages = true;
    $scope.getMsgs = function() {
        $scope.receivedMsgs = Messages.all();
        $scope.sentMsgs = Messages.allSent();
        var linkLogin = "supplierid="+$scope.loginData.userid+"";
        $http.post($scope.domainLink+"/messages/getmessagesbysupplierid", linkLogin).then(function (res){
            var tmpMsgs = res.data;
            var msgsMember = [];
            var msgsMemberSent = [];
            for(var a=0; a<tmpMsgs.length; a++){
                if(tmpMsgs[a].supplierid!=tmpMsgs[a].sender){
                    msgsMember.push(tmpMsgs[a]);
                    // console.log("MESSAGE TO SUPPLIER");
                    // console.log(tmpMsgs[a]);
                }else{
                    msgsMemberSent.push(tmpMsgs[a]);
                }
            }
            if ($scope.receivedMsgs.length != msgsMember.length)  {  
                console.log($scope.receivedMsgs.length +" = "+msgsMember.length);

                if(msgsMember.length>$scope.receivedMsgs.length){
                    console.log('NEW MESSAGE');
                    if($scope.firstLoadMessages){
                        $scope.firstLoadMessages = false;
                    } else {
                        $ionicPopup.alert({
                        title: 'New message received',
                        template: ''
                        });
                    }
                    
                    $ionicScrollDelegate.scrollBottom(true);
                }

                Messages.setMsgs(msgsMember);
                $scope.receivedMsgs = Messages.all();

            } else if ($scope.sentMsgs.length != msgsMemberSent.length){
                Messages.setMsgsSent(msgsMemberSent);
                $scope.sentMsgs = Messages.allSent();

            }
        })
        .finally(function(){
            $ionicLoading.hide();
        });
    }

    $scope.Timer = null;
    //Timer start function.
    $scope.StartTimerMsg = function() {
        $scope.Timer = $interval(function() {
            $scope.getMsgs();
        }, 2000);
    }

    //Timer stop function.
    $scope.StopTimerMsg = function() {

        //Set the Timer stop message.
        // $scope.Message = "Timer stopped.";

        //Cancel the Timer.
        if (angular.isDefined($scope.Timer)) {
            $interval.cancel($scope.Timer);
        }
    }

    $scope.$on("$destroy", function() {
        $scope.StopTimerMsg();
    });

});