angular.module('starter.controllers')
.controller('MyJobsCtrl', function($scope,$ionicLoading, $stateParams,$interval, $ionicPopup ,$http, MyJobs) {
	$scope.jobs = [];
	$ionicLoading.show({
		template: 'Loading Jobs...'
	})
	
	$scope.getMyJobs = function() {
		var linkGetJobs = "id="+$scope.loginData.userid;
            $http.post($scope.domainLink+"/supplier/getjobsbysupplierid", linkGetJobs).then(function (res){
            $scope.response = res;
            var tmp1 = res.data;
            if($scope.response.data.error == true){
              $scope.showAlert(res.data.Message); 
            }else{
                if ($scope.jobs.length != tmp1.length) {
                MyJobs.setJobs(tmp1);
                $scope.jobs = MyJobs.all();
                console.log(tmp1);
                }
            }
        }).finally(function(){
        $ionicLoading.hide();
	});
		};

    $scope.showMore = function(data) {
        $scope.jobClicked = data;
        console.log("clicked data");
        console.log(data);
        console.log($scope.jobClicked);
        //console.log($scope.jobClicked.id);

        console.log($scope.jobClicked);
        var alertPopup = $ionicPopup.alert({
            title: 'Job Information',
            template: "Job ID: "+$scope.jobClicked.id + " - " + $scope.jobClicked.jobname +"<br>Supplier: "+$scope.jobClicked.supplier.email+ "<br><br>Description: " + $scope.jobClicked.description+"<br>Jobtype: "+$scope.jobClicked.jobtype.description
        });

    }
	 $scope.Timer = null;
            //Timer start function.
            $scope.StartTimer = function () {
                $scope.Timer = $interval(function () {
                    $scope.getMyJobs();
                }, 1000);
            };
 
            //Timer stop function.
            $scope.StopTimer = function () {
 
                //Set the Timer stop message.
                // $scope.Message = "Timer stopped.";
 
                //Cancel the Timer.
                if (angular.isDefined($scope.Timer)) {
                    $interval.cancel($scope.Timer);
                }
            };

    $scope.$on("$destroy", function(){
        $scope.StopTimer();
    });
});