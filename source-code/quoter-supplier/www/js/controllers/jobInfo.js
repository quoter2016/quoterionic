angular.module('starter.controllers')
.controller('JobInformationCtrl', function($scope,$location,$timeout, $stateParams,$ionicLoading, $interval, $ionicPopup, $http, FindJobs) {
    $scope.jobInfo = $scope.jobDataClicked.data;
    console.log("JOB INFORMATION: ");
    console.log($scope.jobDataClicked.data);

    $("#jobDescription").val($scope.jobDataClicked.data.description);

    $scope.bidToJob = function() {
            var bidPop = $ionicPopup.confirm({
                title: 'Bid Price',
                template: '<div><br><input id="bidPrice" maxlength="1000" type="number" placeholder=" Input bid price"></input><br><textarea id="bidMsg" maxlength="1000" rows="4" type="text" placeholder=" Message/ Bid details"></textarea></div>',
                cancelText: 'Cancel',
                okText: 'Bid'
            });
            bidPop.then(function(res) {
                if(res) {
                    var bidData = $("#bidPrice").val();
                    var messageData = $("#bidMsg").val();
                    if(bidData.length == 0 || bidData.length == 0){
                        $ionicPopup.alert({
                         title: 'All fields are required.<br>Bid not sent.',
                         template: ''
                       });
                    } else {

                    var fullBidMsg = "Bid Price: "+bidData+" | Bid Details: "+messageData;
                    var linkSendBid= "memberid="+$scope.jobDataClicked.data.member.id+"&supplierid="+$scope.loginData.userid+"&sender="+$scope.loginData.userid+"&content="+fullBidMsg+"&jobid="+$scope.jobDataClicked.data.id+"";
                    $http.post($scope.domainLink+"/messages/sendmessage", linkSendBid).then(function(res) {
                        if(res.data.Status=="Success"){
                            $ionicLoading.hide();
                            $ionicLoading.show({
                              template: 'Bid sent successfully'
                            })
                            $timeout(function() {
                                $ionicLoading.hide();
                            }, 1000);
                        } else {
                            $ionicLoading.hide();
                            $ionicLoading.show({
                              template: 'Error sending bid.'
                            })
                            $timeout(function() {
                                $ionicLoading.hide();
                            }, 1000);
                        }
                    })
                    .finally(function(){
                    });
                    }
                } else {
                    //console.log('dnt delete');

                }
            })
    }

});