angular.module('starter.controllers')
.controller('SupplierMenuCtrl', function($scope,$location, $ionicPopup, $stateParams) {

    	$scope.goMyJobs = function() {
            $location.path('/app/my-jobs');
        }
        $scope.goFindJobs = function() {
            $location.path('/app/find-jobs');
     
        }
        $scope.goMessages = function() {
            $location.path('/app/messages');
        }
        $scope.goPreferences = function() {
            $location.path('/app/preferences');
        }
});